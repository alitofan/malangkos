	<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//frontend url

Route::get('/','frontendController@TampilAll');

route::get('/beranda',function(){
	return view('beranda');
});

Route::get('/about', function() {
	return view('about');
});

Route::get('/contact',function() {
	return view('contact');
});

Route::get('/login',function() {
	return view('login');
});

Route::get('/regristrasi',function() {
	return view('regristrasi');
});

Route::get('/contact', function() {
	return view('contact');
});

Route::get('/berandakos',function(){
	return view('berandakos');
});

Route::get('/detailkos',function(){
	return view('detailkos');
});
// end frontend







// admin url
Route::get('/admin', 'adminController@index');

Route::get('/admin/pemilikkos', 'adminController@Halpemilikkos');
Route::get('/admin/pemilikkos/tambah', 'pemilikController@tambahpemilikkos');
Route::post('/admin/pemilikkos/tambah', 'pemilikController@simpanpemilikkos');
Route::get('/admin/pemilikkos/update-{ID}', 'pemilikController@perUpdate');
Route::post('/admin/pemilikkos/update1', 'pemilikController@update1');
Route::post('/admin/pemilikkos/update2', 'pemilikController@update2');
Route::post('/admin/pemilikkos/update3', 'pemilikController@update3');
Route::get('/admin/pemilikkos/hapus-{ID}', 'pemilikController@hapus');

Route::get('/admin/datakamar', 'adminController@Haldatakamar');
Route::get('/admin/datakamar/tambah', 'datakamarController@frmtambah');
Route::post('/admin/datakamar/tambah', 'datakamarController@insert');
Route::get('/admin/datakamar/update-{ID}', 'datakamarController@perUpdate');
Route::post('/admin/datakamar/update1', 'datakamarController@update1');
Route::post('/admin/datakamar/update2', 'datakamarController@update2');
Route::get('/admin/datakamar/hapus-{ID}', 'datakamarController@hapus');



Route::get('/admin/dataadmin', 'adminController@Haldataadmin');
Route::get('/admin/dataadmin/tambah', 'dataadminController@tambahadmin');
Route::post('/admin/dataadmin/tambah', 'dataadminController@simpanadmin');
Route::get('/admin/dataadmin/update-{ID}', 'dataadminController@perUpdate');
Route::post('/admin/dataadmin/update1', 'dataadminController@update1');
Route::get('/admin/dataadmin/hapus-{ID}', 'dataadminController@hapus');

Route::get('/admin/galeri', 'adminController@Halgaleri');
Route::get('/admin/galeri/tambah', 'galeriController@tambah');
Route::post('/admin/galeri/tambah', 'galeriController@simpan');
Route::get('/admin/galeri/update-{ID}', 'galeriController@perUpdate');
Route::post('/admin/galeri/update1', 'galeriController@update1');
Route::get('/admin/galeri/hapus-{ID}', 'galeriController@hapus');

Route::get('/admin/slide', 'adminController@Halslide');
Route::get('/admin/slide/tambah', 'slideController@tambah');
Route::post('/admin/slide/tambah', 'slideController@simpan');
Route::get('/admin/slide/update-{Id}', 'slideController@perUpdate');
Route::post('/admin/slide/update1', 'slideController@update1');
Route::post('/admin/slide/update2', 'slideController@update2');
Route::get('/admin/slide/hapus-{Id}', 'slideController@hapus');

Route::get('/admin/komplen', 'adminController@Halkomplen');
Route::get('/admin/testimoni', 'adminController@Haltestimoni');
// end of admin url
