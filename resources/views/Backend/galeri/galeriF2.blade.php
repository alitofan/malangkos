@extends('Backend/backend')
@section('act-galeri','active')
@section('title')
<a class="navbar-brand" href="{{url('/admin/galeri')}}">Galeri</a> | <a class="navbar-brand" href="{{url('/admin/galeri/update-'.$tbgalery->IdGalery)}}">Update Data</a>
@endsection
@section('content')
<div class="row">
<div class="col-lg-12">
 
<div class="card">
    <div class="card-header card-header-primary">
        <h4 class="card-title">Data Galeri </h4>
    </div>
    <div class="card-body">
    <center> <img src="{{asset('images/galeri/'.$tbgalery->foto_galery)}}" class="img img[-responsive" width="80%" alt="" srcset=""><br><br>
    <form action="{{url('admin/galeri/update1')}}" method="post" enctype="multipart/form-data">
        <input type="hidden" name="idGaleri" value="{{$tbgalery->IdGalery}}">
        @csrf
        @method('post')
        Ubah Gambar : 
        <input type="file" name="gbrGaleri" id="gbrGaleri"></center>  
        @if ($errors->has('gbrHasilCrop'))
        <small class="text-danger">{{ $errors->first('gbrHasilCrop') }}</small> 
        @endif
        <div id="gbr-galeriCrop"></div>
        <button id="loadCrop_" class="btn btn-primary btn-sm">CROP</button> 
        <input type="hidden" name="gbrHasilCrop" id="gbrHasilCrop">
        @if ($errors->has('gbrHasilCrop'))
            <small class="text-danger">{{ $errors->first('gbrHasilCrop') }}</small> 
        @endif
        <span id="loadCrop" class="text-success">
            {{-- <small >Tunggu Sebentar ...</small>
            <i class="material-icons display-4">done</i> --}}
        </span>
        <br>
        <br>
        <button class="btn btn-primary" id="btnSimpanGaleri" type="submit">SIMPAN</button>
    </form>
    </div>
</div>

    

</div>
</div>
@endsection

@section('js')
<script>
    var sukses = 1;
    if(sukses = {{Session::get('status')}}){
        md.notif("top","right", "Berhasil ...", "info");
    }else{
        md.notif("top","right", "Gagal ...", "danger");
    }
</script>

<script>
$(document).ready(function(){

    // CKEDITOR.replace("keterangan-slide");

    $('#btnSimpanGaleri').hide();

    $uploadCrop = $('#gbr-galeriCrop').croppie({
    enableExif: true,
    viewport: {
        width: 950,
        height: 450,
        type: 'box'
    },
    boundary: {
        width: 980,
        height: 500
    }
    });

    $('#gbrGaleri').on('change', function () { 
    var reader = new FileReader();
        reader.onload = function (e) {
        $uploadCrop.croppie('bind', {
            url: e.target.result
        }).then(function(){
            console.log('jQuery bind complete');
            $('#loadCrop_').show();
        });
        }
        reader.readAsDataURL(this.files[0]);
    });


    $('#loadCrop_').click(function(e){
        e.preventDefault();
        // alert($('#gbrLoad'))
        if($('#gbrGaleri').val() == ""){
            alert("Gambar belum terpilih");
        }else{
            $('#loadCrop').html("<small >Tunggu Sebentar ...</small>");
            $uploadCrop.croppie('result', {
                type: 'canvas',
                size: 'viewport'
            }).then(function (resp) {
                // alert(resp);
                $('#loadCrop').html("<i class='material-icons display-4'>done</i>");
                $('#gbrHasilCrop').val(resp);
                $('#loadCrop_').hide();
                $('#btnSimpanGaleri').show();
            });
        }
    });
});
</script>
@endsection