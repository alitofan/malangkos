@extends('Backend/backend')
@section('act-datakamar','active')
@section('title')
    <a class="navbar-brand" href="{{url('/admin/datakamar')}}">Data Kamar</a>
@endsection
@section('content')

<a class="btn btn-primary btn-sm" href="{{url('/admin/datakamar/tambah')}}">Tambah Data</a>
<div class="row">
<div class="col-lg-12">

<div class="card">
<div class="card-header card-header-primary">
    <h4 class="card-title ">Data Kamar</h4>
    <p class="card-category">
        {{ $kamar->count() == 0 ? "Tidak ada data yang ditampilkan" : $kamar->count()." data sedang ditampilkan" }}
    </p>
</div>
<div class="card-body">
    <div class="table-responsive">
    <table class="table" style="width:100%" id="example">
        <thead class=" text-primary">
            <th width="5%">#</th>
            <th width="25%">Nama Kamar</th>
            <th width="15%">Ukuran</th>
            <th width="15%">Status</th>
            <th width="20%">Pemilik</th>
            <th width="20%" class="text-center">A K S I</th>
        </thead>
        <tbody>
        @php
            $no=1;
        @endphp
        @foreach ($kamar as $kamar)
        <tr>
            <td>{{$no}}</td>
            <td>{{$kamar->nm_kamar}}</td>
            <td>{{$kamar->ukuran}}</td>
            <td>{{$kamar->status == 1 ? "Tersedia" : "Tersewa"}}</td>
            <td>{{$kamar->get_pemilik->nm_Pemilikkos}}</td>
            <td class="text-center">
                <a rel="tooltip" title="Edit" class="btn btn-primary btn-link btn-sm" href="{{url('/admin/datakamar/update-'.$kamar->IdKamar)}}"><i class="material-icons" >edit</i></a>
                
                <button type="button" rel="tooltip" title="Delete" class="btn btn-danger btn-link btn-sm"><i class="material-icons" onclick="hapusKamar('{{url('/admin/datakamar/hapus-'.$kamar->IdKamar)}}')">close</i></button>
            </td>
        </tr>
        @php
            $no++;
        @endphp
        @endforeach
    </tbody>
    </table>

    </div>
</div>
</div>

</div>
</div>



<div class="modal" id="modalDelete">
<div class="modal-dialog">
    <div class="modal-content">

    <!-- Modal Header -->
    <div class="modal-header">
        <h4 class="modal-title">Konfirmasi</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
    </div>

    <!-- Modal body -->
    <div class="modal-body">
        Anda Yakin ?
    </div>

    <!-- Modal footer -->
    <div class="modal-footer">
        <a class="btn btn-primary" href="" id="btnYa">YA</a>
    </div>

    </div>
</div>
</div>
@endsection

@section('js')
<script>
$(document).ready(function(){
    $('#example').DataTable();

    
});
function hapusKamar(idPemilik){
    $('#btnYa').attr("href", idPemilik);
    $('#modalDelete').modal();
}
</script>
<script>
var sukses = 1;
    if(sukses = {{Session::get('status')}}){
        md.notif("top","right", "Berhasil ...", "info");
    }else{
        md.notif("top","right", "Gagal ...", "danger");
    }
</script>
@endsection