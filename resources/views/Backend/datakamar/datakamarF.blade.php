@extends('Backend/backend')
@section('act-datakamr','active')
@section('title')
    <a class="navbar-brand" href="{{url('/admin/datakamar/')}}">Data Kamar</a> | <a class="navbar-brand" href="{{url('/admin/datakamar/tambah')}}">Tambah Data</a>
@endsection
@section('content')

<div class="row">
<div class="col-lg-12">

<div class="card">
<div class="card-header card-header-primary">
    <h4 class="card-title">Data Kamar </h4>
    <p class="card-category">Lengkapi data</p>
</div>
<div class="card-body">
<form action="{{url('admin/datakamar/tambah')}}" method="post" enctype="multipart/form-data">
    @csrf
    @method('post')
    <div class="row">
    <div class="col-lg-6">
        <div class="form-group">
            <label class="bmd-label-floating">Nama Pemilik Kos</label>
            <select name="nmPemilik" id="nmPemilik" class="form-control">
                <option value="">[ PILIH ]</option>
                @foreach ($pemilik as $pemilik)
                <option value="{{$pemilik->IdPemilik_kos}}">{{$pemilik->nm_Pemilikkos}}</option>    
                @endforeach
            </select>
            @if ($errors->has('nmPemilik'))
                <small class="text-danger">{{ $errors->first('nmPemilik') }}</small> 
            @endif
        </div>
        <div class="form-group">
            <label class="bmd-label-floating">Nama Kamar</label>
            <input type="text" class="form-control" name="nmKamar" id="nm-kamar" value="{{old('nmKamar')}}">
            @if ($errors->has('nmKamar'))
                <small class="text-danger">{{ $errors->first('nmKamar') }}</small> 
            @endif
        </div>
        <div class="form-group">
            <label class="bmd-label-floating">Ukuran Kamar</label>
            <input type="text" class="form-control" name="ukuranKamar" id="ukuran-kamar" value="{{old('ukuranKamar')}}">
            @if ($errors->has('ukuranKamar'))
                <small class="text-danger">{{ $errors->first('ukuranKamar') }}</small> 
            @endif
        </div>
        <div class="form-group">
            <label class="bmd-label-floating">Fasilitas Kamar</label>
            <input type="text" class="form-control" name="fasilitasKamar" id="fasilitas-kamar" value="{{old('fasilitasKamar')}}">
            @if ($errors->has('fasilitasKamar'))
                <small class="text-danger">{{ $errors->first('fasilitasKamar') }}</small> 
            @endif
        </div>
    </div>

    <div class="col-lg-6">
        <div class="form-group">
            <label class="bmd-label-floating">Harga Kamar</label>
            <textarea name="hargaKamar" id="harga-kamar" cols="30" rows="5" class="form-control">
@if (old('hargaKamar') <> "")
{{old('hargaKamar')}}
@else
Per Bulan : -
Per Tahun   : - 
Per Semester    : -
Per Hari : -
                @endif
            </textarea>
            @if ($errors->has('hargaKamar'))
                <small class="text-danger">{{ $errors->first('hargaKamar') }}</small> 
            @endif
        </div>
        
        Foto Kamar
        <input type="file" name="fotoKamar" id="" >
        @if ($errors->has('fotoKamar'))
        <br><small class="text-danger">{{ $errors->first('fotoKamar') }}</small> 
        @endif

    
        
    </div>
    
    </div>
    <div class="row">
    <div class="col-lg-12">
        <button class="btn btn-sm btn-primary">S I M P A N</button>
    </div>
    </div>
</form>
</div>
</div>



</div>
</div>
@endsection

@section('css')
<link rel="stylesheet" href="{{asset('/Bend/assets/css/select2.min.css')}}">
@endsection
@section('js')
<script src="{{asset('/Bend/assets/js/select2.min.js')}}"></script>
<script>
$(document).ready(function(){
    $('#nmPemilik').select2();

    var sukses = 1;
    if(sukses = {{Session::get('status')}}){
        md.notif("top","right", "Berhasil ...", "info");
    }else{
        md.notif("top","right", "Gagal ...", "danger");
    }

});
</script>    
@endsection