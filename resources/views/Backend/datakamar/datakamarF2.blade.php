@extends('Backend/backend')
@section('act-datakamr','active')
@section('title')
<a class="navbar-brand" href="{{url('/admin/datakamar/')}}">Data Kamar</a> | <a class="navbar-brand" href="{{url('/admin/datakamar/update-'.$tbkamar->IdKamar)}}">Update Data Kamar {{$tbkamar->get_pemilik->nm_Pemilikkos}}</a>
@endsection
@section('content')

<div class="row">
<div class="col-lg-12">

<div class="card">
<div class="card-header card-header-primary">
    <h4 class="card-title">Data Text </h4>
    <p class="card-category">Lengkapi data</p>
</div>
<div class="card-body">
<form action="{{url('admin/datakamar/update1')}}" method="post" enctype="multipart/form-data">
    @csrf
    @method('post')
    <input type="hidden" name="idKamar" id="idKamar" value="{{$tbkamar->IdKamar}}">
    <div class="row">
    <div class="col-lg-6">
        <div class="form-group">
            <label class="bmd-label-floating">Nama Pemilik Kos</label>
            @if ($errors->has('nmPemilik'))
                <select name="nmPemilik" id="nmPemilik" class="form-control">
                    <option value="">[ PILIH ]</option>
                    @foreach ($tbpemilik as $pemilik)
                    <option value="{{$pemilik->IdPemilik_kos}}" {{ $pemilik->IdPemilik_kos == old('nmPemilik') ? "selected" : "" }}>{{old('nmPemilik')}}</option>    
                    @endforeach
                </select>
                <small class="text-danger">{{ $errors->first('nmPemilik') }}</small> 
            @else
                <select name="nmPemilik" id="nmPemilik" class="form-control">
                    <option value="">[ PILIH ]</option>
                    @foreach ($tbpemilik as $pemilik)
                    <option value="{{$pemilik->IdPemilik_kos}}" {{ $pemilik->IdPemilik_kos == $tbkamar->IdPemilik_kos ? "selected" : "" }}>{{$pemilik->nm_Pemilikkos}}</option>    
                    @endforeach
                </select>
            @endif
        </div>
        <div class="form-group">
            <label class="bmd-label-floating">Nama Kamar</label>
            @if ($errors->has('nmKamar'))
                <input type="text" class="form-control" name="nmKamar" id="nm-kamar" value="{{old('nmKamar')}}">
                <small class="text-danger">{{ $errors->first('nmKamar') }}</small>
                @else
                <input type="text" class="form-control" name="nmKamar" id="nm-kamar" value="{{$tbkamar->nm_kamar}}">
            @endif
        </div>
        <div class="form-group">
            <label class="bmd-label-floating">Ukuran Kamar</label>
            @if ($errors->has('ukuranKamar'))
                <input type="text" class="form-control" name="ukuranKamar" id="ukuran-kamar" value="{{old('ukuranKamar')}}">
                <small class="text-danger">{{ $errors->first('ukuranKamar') }}</small> 
            @else
                <input type="text" class="form-control" name="ukuranKamar" id="ukuran-kamar" value="{{$tbkamar->ukuran}}">
            @endif
        </div>
        <div class="form-group">
            <label class="bmd-label-floating">Fasilitas Kamar</label>
            @if ($errors->has('fasilitasKamar'))
                <input type="text" class="form-control" name="fasilitasKamar" id="fasilitas-kamar" value="{{old('fasilitasKamar')}}">
                <small class="text-danger">{{ $errors->first('fasilitasKamar') }}</small> 
            @else
                <input type="text" class="form-control" name="fasilitasKamar" id="fasilitas-kamar" value="{{$tbkamar->fasilitas}}">
            @endif
        </div>
        <div class="form-group">
            <label class="bmd-label-floating">Status Kamar</label>
            <select name="statusKamar" id="statusKamar" class="form-control">
                <option value="">[ PILIH ]</option>
                @for ($i = 1; $i <= 2; $i++)
                    @if (old('statusKamar') == "")
                        <option value="{{$i}}" {{$i == $tbkamar->status ? "selected" : ""}}>{{$i == 1 ? "Tersedia" : "Tersewa"}}</option>    
                    @else
                        <option value="{{$i}}" {{$i == old("status") ? "selected" : ""}}>{{$i == 1 ? "Tersedia" : "Tersewa"}}</option>
                    @endif
                        
                @endfor
            </select>
            @if ($errors->has('statusKamar'))
                <small class="text-danger">{{ $errors->first('statusKamar') }}</small> 
            @endif
        </div>
    </div>

    <div class="col-lg-6">
        <div class="form-group">
            <label class="bmd-label-floating">Harga Kamar</label>
            <textarea name="hargaKamar" id="harga-kamar" cols="30" rows="5" class="form-control">{{old('hargaKamar') == "" ? $tbkamar->harga : old('hargaKamar') }}</textarea>
            @if ($errors->has('hargaKamar'))
                <small class="text-danger">{{ $errors->first('hargaKamar') }}</small> 
            @endif
        </div>
        
      
        
    </div>
    
    </div>
    <div class="row">
    <div class="col-lg-12">
        <button class="btn btn-sm btn-primary">S I M P A N</button>
    </div>
    </div>
</form>
</div>
</div>


<div class="card">
    <div class="card-header card-header-primary">
        <h4 class="card-title">Foto Kamar</h4>
        <p class="card-category">Lengkapi data</p>
    </div>
    <div class="card-body">
    <form action="{{url('admin/datakamar/update2')}}" method="post" enctype="multipart/form-data">
        @csrf
        @method('post')
        <input type="hidden" name="idKamar" value="{{$tbkamar->IdKamar}}">
        <input type="hidden" name="oldFotoKamar" value="{{$tbkamar->foto_kamar}}">
        <div class="row">
            <div class="col-lg-6">
                <img src="{{asset('images/kamar/'.$tbkamar->foto_kamar)}}" class="img img-responsive" width="100%" alt="" srcset="">
            </div>
            <div class="col-lg-6">
                Ganti foto kamar
                <input type="file" name="fotoKamar" id="" >
                @if ($errors->has('fotoKamar'))
                <br><small class="text-danger">{{ $errors->first('fotoKamar') }}</small> 
                @endif
                <br><button class="btn btn-sm btn-primary">S I M P A N</button>
            </div>
        </div>

    </form>
    </div>
</div>



</div>
</div>
@endsection

@section('css')
<link rel="stylesheet" href="{{asset('/Bend/assets/css/select2.min.css')}}">
@endsection
@section('js')
<script src="{{asset('/Bend/assets/js/select2.min.js')}}"></script>
<script>
$(document).ready(function(){
    $('#nmPemilik').select2();

    var sukses = 1;
    if(sukses = {{Session::get('status')}}){
        md.notif("top","right", "Berhasil ...", "info");
    }else{
        md.notif("top","right", "Gagal ...", "danger");
    }

});
</script>    
@endsection