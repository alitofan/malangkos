@extends('Backend/backend')
@section('act-dashboard','active')
@section('title')
    <a class="navbar-brand" href="{{url('/admin')}}">Dashboard</a>
@endsection
@section('content')
<div class="row">
    <div class="col-lg-4">
        <div class="card card-stats">
        <div class="card-header card-header-success card-header-icon">
            <div class="card-icon">
            <i class="material-icons">store</i>
            </div>
            <p class="card-category">Jumlah Kos-kosan terdaftar</p>
            <h3 class="card-title">1540</h3>
        </div>
        <div class="card-footer">
            <div class="stats">
            <i class="material-icons">date_range</i> Update tanggal 12/12/2018
            </div>
        </div>
        </div>
    </div>
    <div class="col-lg-4">
        <div class="card card-stats">
        <div class="card-header card-header-success card-header-icon">
            <div class="card-icon">
            <i class="material-icons">store</i>
            </div>
            <p class="card-category">Jumlah kunjungan bulan ini</p>
            <h3 class="card-title">1540</h3>
        </div>
        <div class="card-footer">
            <div class="stats">
            <i class="material-icons">date_range</i> Update tanggal 12/12/2018
            </div>
        </div>
        </div>
    </div>
    <div class="col-lg-4">
        <div class="card card-stats">
        <div class="card-header card-header-success card-header-icon">
            <div class="card-icon">
            <i class="material-icons">store</i>
            </div>
            <p class="card-category">Jumlah kos-kosan disetujui</p>
            <h3 class="card-title">1540</h3>
        </div>
        <div class="card-footer">
            <div class="stats">
            <i class="material-icons">date_range</i> Update tanggal 12/12/2018
            </div>
        </div>
        </div>
    </div>
    <div class="col-lg-4">
        <div class="card card-stats">
        <div class="card-header card-header-success card-header-icon">
            <div class="card-icon">
            <i class="material-icons">store</i>
            </div>
            <p class="card-category">Kos-kosan belum disetujui</p>
            <h3 class="card-title">1540</h3>
        </div>
        <div class="card-footer">
            <div class="stats">
            <i class="material-icons">date_range</i> Update tanggal 12/12/2018
            </div>
        </div>
        </div>
    </div>
</div>
@endsection