@extends('Backend/backend')
@section('act-pemilikkos','active')
@section('title')
    <a class="navbar-brand" href="{{url('/admin/pemilikkos')}}">Pemilik Kos</a> | <a class="navbar-brand" href="{{url('/admin/pemilikkos/update-'.$pemilik->IdPemilik_kos)}}">Edit Data</a>
@endsection
@section('content')


<div class="row">
<div class="col-lg-12">

<div class="card">
    <div class="card-header card-header-primary">
        <h4 class="card-title">Data Teks</h4>
        <p class="card-category">Lengkapi data</p>
    </div>
    <div class="card-body">
    <form action="{{url('admin/pemilikkos/update1')}}" method="post" enctype="multipart/form-data">
        @csrf
        @method('post')
        <input type="hidden" name="IdPemilik" value="{{$pemilik->IdPemilik_kos}}">
        <div class="row">
        <div class="col-lg-6">
            <div class="form-group">
                <label class="bmd-label-floating">Nama Pemilik Kos</label>
                <input type="text" class="form-control" name="nmPemilikkos" id="nm-pemilikkos" value="{{ old('nmPemilikkos') == "" ? $pemilik->nm_Pemilikkos : old('nmPemilikkos') }}">
                @if ($errors->has('nmPemilikkos'))
                    <small class="text-danger">{{ $errors->first('nmPemilikkos') }}</small> 
                @endif
            </div>
            <div class="form-group">
                <label class="bmd-label-floating">Alamat Pemilik Kos</label>
                <input type="text" class="form-control" name="alamatPemilikkos" id="alamat-pemilikkos" value="{{old('alamatPemilikkos') == "" ? $pemilik->alamat : old('alamatPemilikkos') }}">
                @if ($errors->has('alamatPemilikkos'))
                    <small class="text-danger">{{ $errors->first('alamatPemilikkos') }}</small> 
                @endif
            </div>
            <div class="form-group">
                <label class="bmd-label-floating">Telp Pemilik Kos</label>
                <input type="text" class="form-control" name="telpPemilikkos" id="telp-pemilikkos" value="{{old('telpPemilikkos') == "" ? $pemilik->no_telp : old('telpPemilikkos') }}">
                @if ($errors->has('telpPemilikkos'))
                    <small class="text-danger">{{ $errors->first('telpPemilikkos') }}</small> 
                @endif
            </div>
            <div class="form-group">
                <label class="bmd-label-floating">WA Pemilik Kos</label>
                <input type="text" class="form-control" name="waPemilikkos" id="wa-pemilikkos" value="{{old('waPemilikkos') == "" ? $pemilik->no_wa : old('waPemilikkos') }}">
                @if ($errors->has('waPemilikkos'))
                    <small class="text-danger">{{ $errors->first('waPemilikkos') }}</small> 
                @endif
            </div>
            
            <div class="form-group">
                <label class="bmd-label-floating">Status</label>
                <select name="status" id="status" class="form-control">
                    <option value="" disabled >[ P I L I H ]</option>
                    <option value="AKTIF" {{ $pemilik->status == "AKTIF" ? "selected" : "" }} >AKTIF</option>
                    <option value="PASIF"  {{ $pemilik->status == "PASIF" ? "selected" : "" }}>PASIF</option>
                </select>
                @if ($errors->has('status'))
                    <small class="text-danger">{{ $errors->first('status') }}</small> 
                @endif
            </div>
        </div>
        <div class="col-lg-6">
            <div class="form-group">
                <label class="bmd-label-floating">Keterangan Pemilik Kos</label>
                <textarea name="ketPemilikkos" id="ket-pemilikkos" cols="30" rows="5" class="form-control">{{old('ketPemilikkos') == "" ? $pemilik->ket_pemilik : old('ketPemilikkos')}}</textarea>
                @if ($errors->has('ketPemilikkos'))
                    <small class="text-danger">{{ $errors->first('ketPemilikkos') }}</small> 
                @endif
            </div>
            
        </div>
        
        </div>
        <div class="row">
        <div class="col-lg-12">
            <button class="btn btn-sm btn-primary">S I M P A N</button>
        </div>
        </div>
    </form>
    </div>
</div>

<div class="card">
    <div class="card-header card-header-primary">
        <h4 class="card-title">Foto KTP</h4>
        <p class="card-category">Lengkapi data</p>
    </div>
    <div class="card-body">
    <form action="{{url('admin/pemilikkos/update2')}}" method="post" enctype="multipart/form-data">
        @csrf
        @method('post')
        <input type="hidden" name="IdPemilik" value="{{$pemilik->IdPemilik_kos}}">
        <div class="row">
            <div class="col-lg-6">
                <img src="{{asset('images/pemilikkos/'.$pemilik->foto_ktp)}}" class="img img-responsive" width="100%" alt="" srcset="">
            </div>
            <div class="col-lg-6">
                Ganti foto KTP
                <input type="file" name="fotoKtp" id="" >
                @if ($errors->has('fotoKtp'))
                <br><small class="text-danger">{{ $errors->first('fotoKtp') }}</small> 
                @endif
                <br><button class="btn btn-sm btn-primary">S I M P A N</button>
            </div>
        </div>

    </form>
    </div>
</div>
    

    <div class="card">
            <div class="card-header card-header-primary">
                <h4 class="card-title">Foto Perjanjian</h4>
                <p class="card-category">Lengkapi data</p>
            </div>
            <div class="card-body">
                <form action="{{url('admin/pemilikkos/update3')}}" method="post" enctype="multipart/form-data">
                    @csrf
                    @method('post')
                    <input type="hidden" name="IdPemilik" value="{{$pemilik->IdPemilik_kos}}">
                    <div class="row">
                        <div class="col-lg-6">
                            <img src="{{asset('images/pemilikkos/'.$pemilik->foto_perjanjian)}}" class="img img-responsive" width="100%" alt="" srcset="">
                        </div>
                        <div class="col-lg-6">
                            Ganti foto perjanjian
                            <input type="file" name="fotoPerjanjian" id="" >
                            @if ($errors->has('fotoPerjanjian'))
                            <br><small class="text-danger">{{ $errors->first('fotoPerjanjian') }}</small> 
                            @endif
                            <button class="btn btn-sm btn-primary">S I M P A N</button>
                        </div>
                    </div>
                   
                </form>
            </div>
        </div>



</div>
</div>


@endsection
@section('js')
<script>
$(document).ready(function(){
    var sukses = 1;
    if(sukses = {{Session::get('status')}}){
        md.notif("top","right", "Berhasil ...", "info");
    }else{
        md.notif("top","right", "Gagal ...", "danger");
    }
})
</script>
    
@endsection