@extends('Backend/backend')
@section('act-pemilikkos','active')
@section('title')
    <a class="navbar-brand" href="{{url('/admin/pemilikkos')}}">Pemilik Kos</a>
@endsection
@section('content')

<a class="btn btn-primary btn-sm" href="{{url('/admin/pemilikkos/tambah')}}">Tambah Data</a>

<div class="row">
<div class="col-lg-12">

<div class="card">
<div class="card-header card-header-primary">
    <h4 class="card-title ">Data Pemilik KOS</h4>
    <p class="card-category">
        {{ $pemilik->count() == 0 ? "Tidak ada data yang ditampilkan" : $pemilik->count()." data sedang ditampilkan" }}
    </p>
</div>
<div class="card-body">
    <div class="table-responsive">

    <table class="table" style="width:100%" id="example">
        <thead class=" text-primary">
            <th width="5%">#</th>
            <th width="25%">Nama Kosan</th>
            <th width="30%">Alamat</th>
            <th width="15%">Telp</th>
            <th width="15%" class="text-center">A K S I</th>
        </thead>
        <tbody>
        @php
            $no=1;
        @endphp
        @foreach ($pemilik as $pemilik)
        <tr>
            <td>{{$no}}</td>
            <td>{{$pemilik->nm_Pemilikkos}}</td>
            <td>{{$pemilik->alamat}}</td>
            <td>{{$pemilik->no_telp}}</td>
            <td>
                <a rel="tooltip" title="Edit" class="btn btn-primary btn-link btn-sm" href="{{url('/admin/pemilikkos/update-'.$pemilik->IdPemilik_kos)}}"><i class="material-icons" >edit</i></a>
                
                <button type="button" rel="tooltip" title="Delete" class="btn btn-danger btn-link btn-sm"><i class="material-icons" onclick="hapusPemilik('{{url('/admin/pemilikkos/hapus-'.$pemilik->IdPemilik_kos)}}')">close</i></button>
            </td>
        </tr>
        @php
            $no++;
        @endphp
        @endforeach
    </tbody>
    </table>

    </div>
</div>
</div>

</div>
</div>



<!-- The Modal -->
<div class="modal" id="modalDelete">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Konfirmasi</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        Anda Yakin ?
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <a class="btn btn-primary" href="" id="btnYa">YA</a>
      </div>

    </div>
  </div>
</div>

@endsection

@section('css')
<link rel="stylesheet" href="{{asset('Bend/datatable/DataTables-1.10.18/css/dataTables.bootstrap4.min.css')}}">
@endsection

@section('js')

<script src="Bend/datatable/DataTables-1.10.18/js/jquery.dataTables.min.js"></script>
<script src="Bend/datatable/DataTables-1.10.18/js/dataTables.bootstrap4.min.js"></script>
<script>
$(document).ready(function() {
    $('#example').DataTable();
  
});

function hapusPemilik(idPemilik){
    $('#btnYa').attr("href", idPemilik);
    $('#modalDelete').modal();
}
</script>

<script>
    var sukses = 1;
    if(sukses = {{Session::get('status')}}){
        md.notif("top","right", "Berhasil ...", "info");
    }else{
        md.notif("top","right", "Gagal ...", "danger");
    }
</script>

@endsection