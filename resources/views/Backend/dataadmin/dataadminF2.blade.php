@extends('Backend/backend')
@section('act-dataadmin','active')
@section('title')
    <a class="navbar-brand" href="{{url('/admin/dataadmin')}}">Pemilik Kos</a> | <a class="navbar-brand" href="{{url('/admin/dataadmin/update-'.$admin->IdAdmin)}}">Edit Data</a>
@endsection
@section('content')


<div class="row">
<div class="col-lg-12">

<div class="card">
    <div class="card-header card-header-primary">
        <h4 class="card-title">Data Teks</h4>
        <p class="card-category">Lengkapi data</p>
    </div>
    <div class="card-body">
    <form action="{{url('admin/dataadmin/update1')}}" method="post" enctype="multipart/form-data">
        @csrf
        @method('post')
        <input type="hidden" name="IdAdmin" value="{{$admin->IdAdmin}}">
        <div class="row">
        <div class="col-lg-6">
            <div class="form-group">
                <label class="bmd-label-floating">User Name</label>
                <input type="text" class="form-control" name="usernm" id="username" value="{{ old('usernm') == "" ? $admin->username : old('usernm') }}">
                @if ($errors->has('usernm'))
                    <small class="text-danger">{{ $errors->first('usernm') }}</small> 
                @endif
            </div>
            
            <div class="form-group">
                <label class="bmd-label-floating">Otoritas</label>
                <select name="otoritas" id="otoritas" class="form-control">
                    <option value="" disabled >[ P I L I H ]</option>
                    <option value="1" {{ $admin->status == "1" ? "selected" : "" }} >Administator</option>
                    <option value="2"  {{ $admin->status == "2" ? "selected" : "" }}>Admin</option>
                </select>
                @if ($errors->has('otoritas'))
                    <small class="text-danger">{{ $errors->first('otoritas') }}</small> 
                @endif
            </div>
        
        <div class="form-group">
                <label class="bmd-label-floating">Password</label>
                <input type="password" class="form-control" name="pass" id="pass" value="{{old('pass') == "" ? $admin->pass : old('pass') }}">
                @if ($errors->has('pass'))
                    <small class="text-danger">{{ $errors->first('pass') }}</small> 
                @endif
            </div>
        </div>
        
        </div>
        <div class="row">
        <div class="col-lg-12">
            <button class="btn btn-sm btn-primary">S I M P A N</button>
        </div>
        </div>
    </form>
    </div>
</div>

</div>
</div>


@endsection
@section('js')
<script>
$(document).ready(function(){
    var sukses = 1;
    if(sukses = {{Session::get('status')}}){
        md.notif("top","right", "Berhasil ...", "info");
    }else{
        md.notif("top","right", "Gagal ...", "danger");
    }
})
</script>
    
@endsection