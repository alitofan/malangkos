@extends('Backend/backend')
@section('act-dataadmin','active')
@section('title')
    <a class="navbar-brand" href="{{url('/admin/dataadmin')}}">Data Admin</a>
@endsection
@section('content')

<a class="btn btn-primary btn-sm" href="{{url('/admin/dataadmin/tambah')}}">Tambah Data</a>
<div class="row">
<div class="col-lg-12">

<div class="card">
<div class="card-header card-header-primary">
    <h4 class="card-title ">Data Admin</h4>
    <p class="card-category">
        {{ $admin->count() == 0 ? "Tidak ada data yang ditampilkan" : $admin->count()." data sedang ditampilkan" }}
    </p>
</div>
<div class="card-body">
    <div class="table-responsive">
    <table class="table" style="width:100%" id="example">
        <thead class=" text-primary">
            <th width="5%">#</th>
            <th width="25%">User Nmae</th>
            <th width="15%">Otoritas</th>
            <th width="20%" class="text-center">A K S I</th>
        </thead>
        <tbody>
        @php
            $no=1;
        @endphp
        @foreach ($admin as $admin)
        <tr>
            <td>{{$no}}</td>
            <td>{{$admin->username}}</td>
            <td>{{$admin->otoritas == 1 ? "Administrator" : "Admin"}}</td>
            <td class="text-center">
                <a rel="tooltip" title="Edit" class="btn btn-primary btn-link btn-sm" href="{{url('/admin/dataadmin/update-'.$admin->IdAdmin)}}"><i class="material-icons" >edit</i></a>
                
                <button type="button" rel="tooltip" title="Delete" class="btn btn-danger btn-link btn-sm"><i class="material-icons" onclick="hapusAdmin('{{url('/admin/dataadmin/hapus-'.$admin->IdAdmin)}}')">close</i></button>
            </td>
        </tr>
        @php
            $no++;
        @endphp
        @endforeach
    </tbody>
    </table>

    </div>
</div>
</div>

</div>
</div>



<div class="modal" id="modalDelete">
<div class="modal-dialog">
    <div class="modal-content">

    <!-- Modal Header -->
    <div class="modal-header">
        <h4 class="modal-title">Konfirmasi</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
    </div>

    <!-- Modal body -->
    <div class="modal-body">
        Anda Yakin ?
    </div>

    <!-- Modal footer -->
    <div class="modal-footer">
        <a class="btn btn-primary" href="" id="btnYa">YA</a>
    </div>

    </div>
</div>
</div>
@endsection

@section('css')
<link rel="stylesheet" href="{{asset('Bend/datatable/DataTables-1.10.18/css/dataTables.bootstrap4.min.css')}}">
@endsection

@section('js')
<script src="Bend/datatable/DataTables-1.10.18/js/jquery.dataTables.min.js"></script>
<script src="Bend/datatable/DataTables-1.10.18/js/dataTables.bootstrap4.min.js"></script>
<script>
$(document).ready(function() {
    $('#example').DataTable();
  
});

function hapusAdmin(idAdmin){
    $('#btnYa').attr("href", idAdmin);
    $('#modalDelete').modal();
}
</script>
<script>
var sukses = 1;
    if(sukses = {{Session::get('status')}}){
        md.notif("top","right", "Berhasil ...", "info");
    }else{
        md.notif("top","right", "Gagal ...", "danger");
    }
</script>
@endsection