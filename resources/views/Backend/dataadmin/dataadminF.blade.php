@extends('Backend/backend')
@section('act-dataadmin','active')
@section('title')
    <a class="navbar-brand" href="{{url('/admin/dataadmin/')}}">Data Admin</a> | <a class="navbar-brand" href="{{url('/admin/dataadmin/tambah')}}">Tambah Data</a>
@endsection
@section('content')

<div class="row">
<div class="col-lg-12">

<div class="card">
<div class="card-header card-header-primary">
    <h4 class="card-title">Data Admin </h4>
    <p class="card-category">Lengkapi data</p>
</div>
<div class="card-body">
<form action="{{url('admin/dataadmin/tambah')}}" method="post" enctype="multipart/form-data">
    @csrf
    @method('post')
    <div class="row">
    <div class="col-lg-6">
        <div class="form-group">
            <label class="bmd-label-floating">User Name</label>
            <input type="text" class="form-control" name="usernm" id="usernm" value="{{old('usernm')}}">
                @if ($errors->has('usernm'))
                    <small class="text-danger">{{ $errors->first('usernm') }}</small> 
                @endif
        </div>
         <div class="form-group">
            <label class="bmd-label-floating">Otoritas</label>
            <select name="otoritas" id="otoritas" class="form-control">
                <option value="">[ PILIH ]</option> 
                <option value="1">Administrator</option> 
                <option value="2">Admin</option>    
            </select>
            @if ($errors->has('otoritas'))
                <small class="text-danger">{{ $errors->first('otoritas') }}</small> 
            @endif
        </div>
        <div class="form-group">
            <label class="bmd-label-floating">Password</label>
            <input type="password" class="form-control" name="pass" id="pass" value="{{old('pass')}}">
            @if ($errors->has('pass'))
                <small class="text-danger">{{ $errors->first('pass') }}</small> 
            @endif
        </div>
      
    </div>
    </div>
    <div class="row">
    <div class="col-lg-12">
        <button class="btn btn-sm btn-primary">S I M P A N</button>
    </div>
    </div>
</form>
</div>
</div>



</div>
</div>
@endsection

@section('css')
<link rel="stylesheet" href="{{asset('/Bend/assets/css/select2.min.css')}}">
@endsection
@section('js')
<script src="{{asset('/Bend/assets/js/select2.min.js')}}"></script>
<script>
$(document).ready(function(){
    $('#nmPemilik').select2();

    var sukses = 1;
    if(sukses = {{Session::get('status')}}){
        md.notif("top","right", "Berhasil ...", "info");
    }else{
        md.notif("top","right", "Gagal ...", "danger");
    }

});
</script>    
@endsection