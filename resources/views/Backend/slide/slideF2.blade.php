@extends('Backend/backend')
@section('act-slide','active')
@section('title')
<a class="navbar-brand" href="{{url('/admin/slide')}}">Slide</a> | <a class="navbar-brand" href="{{url('/admin/slide/update-'.$tbslide->IdSlide)}}">Update Data</a>
@endsection
@section('content')
<div class="row">
<div class="col-lg-12">
 
<div class="card">
    <div class="card-header card-header-primary">
        <h4 class="card-title">Data slide </h4>
    </div>
    <div class="card-body">
    <form action="{{url('admin/slide/update1')}}" method="post" enctype="multipart/form-data">
        @csrf
        @method('post')
        <input type="hidden" name="idSlide" value="{{$tbslide->IdSlide}}">
        <div class="form-group">
            <label class="bmd-label-floating">Title</label>
            <input type="text" class="form-control" name="title" id="title" value="{{ old('title') !== null ? old('title') : $tbslide->title}}">
            @if ($errors->has('title'))
                <small class="text-danger">{{ $errors->first('title') }}</small> 
            @endif
        </div>

        <div class="form-group">
            <label class="bmd-label-floating">Caption</label>
            <input type="text" class="form-control" name="caption" id="caption" value="{{old('caption') == '' ? $tbslide->caption : old('title')}}">
            @if ($errors->has('caption'))
                <small class="text-danger">{{ $errors->first('caption') }}</small> 
            @endif
        </div>
        <button class="btn btn-primary btn-sm" id="btnSimpanSlide1" type="submit">SIMPAN</button>
    </form>
    </div>
</div>


<div class="card">
    <div class="card-header card-header-primary">
        <h4 class="card-title">Data slide </h4>
    </div>
    <div class="card-body">
    <center> <img src="{{asset('images/slide/'.$tbslide->foto_slide)}}" class="img img[-responsive" width="80%" alt="" srcset=""><br><br>
    <form action="{{url('admin/slide/update2')}}" method="post" enctype="multipart/form-data">
        <input type="hidden" name="idSlide" value="{{$tbslide->IdSlide}}">
        @csrf
        @method('post')
        Ubah Gambar : 
        <input type="file" name="gbrSlide" id="gbrSlide"></center>  
        @if ($errors->has('gbrHasilCrop'))
        <small class="text-danger">{{ $errors->first('gbrHasilCrop') }}</small> 
        @endif
        <div id="gbr-slideCrop"></div>
        <button id="loadCrop_" class="btn btn-primary btn-sm">CROP</button> 
        <input type="hidden" name="gbrHasilCrop" id="gbrHasilCrop">
        @if ($errors->has('gbrHasilCrop'))
            <small class="text-danger">{{ $errors->first('gbrHasilCrop') }}</small> 
        @endif
        <span id="loadCrop" class="text-success">
            {{-- <small >Tunggu Sebentar ...</small>
            <i class="material-icons display-4">done</i> --}}
        </span>
        <br>
        <br>
        <button class="btn btn-primary" id="btnSimpanSlide" type="submit">SIMPAN</button>
    </form>
    </div>
</div>

    

</div>
</div>
@endsection

@section('js')
<script>
    var sukses = 1;
    if(sukses = {{Session::get('status')}}){
        md.notif("top","right", "Berhasil ...", "info");
    }else{
        md.notif("top","right", "Gagal ...", "danger");
    }
</script>

<script>
$(document).ready(function(){

    // CKEDITOR.replace("keterangan-slide");

    $('#btnSimpanSlide').hide();

    $uploadCrop = $('#gbr-slideCrop').croppie({
    enableExif: true,
    viewport: {
        width: 950,
        height: 450,
        type: 'box'
    },
    boundary: {
        width: 980,
        height: 500
    }
    });

    $('#gbrSlide').on('change', function () { 
    var reader = new FileReader();
        reader.onload = function (e) {
        $uploadCrop.croppie('bind', {
            url: e.target.result
        }).then(function(){
            console.log('jQuery bind complete');
            $('#loadCrop_').show();
        });
        }
        reader.readAsDataURL(this.files[0]);
    });


    $('#loadCrop_').click(function(e){
        e.preventDefault();
        // alert($('#gbrLoad'))
        if($('#gbrSlide').val() == ""){
            alert("Gambar belum terpilih");
        }else{
            $('#loadCrop').html("<small >Tunggu Sebentar ...</small>");
            $uploadCrop.croppie('result', {
                type: 'canvas',
                size: 'viewport'
            }).then(function (resp) {
                // alert(resp);
                $('#loadCrop').html("<i class='material-icons display-4'>done</i>");
                $('#gbrHasilCrop').val(resp);
                $('#loadCrop_').hide();
                $('#btnSimpanSlide').show();
            });
        }
    });
});
</script>
@endsection