@extends('Backend/backend')
@section('act-slide','active')
@section('title')
    <a class="navbar-brand" href="{{url('/admin/slide')}}">Slide</a>
@endsection
@section('content')
<a class="btn btn-primary btn-sm" href="{{url('/admin/slide/tambah')}}">Tambah Data</a>

<div class="row">
<div class="col-lg-12">

<div class="card">
<div class="card-header card-header-primary">
    <h4 class="card-title ">Data Slide</h4>
    <p class="card-category">
        {{ $tbslide->count() == 0 ? "Tidak ada data yang ditampilkan" : $tbslide->count()." data sedang ditampilkan" }}
    </p>
</div>
<div class="card-body">
    <div class="table-responsive">

    <table class="table" style="width:100%" id="example">
        <thead class=" text-primary">
            <th width="5%">#</th>
            <th width="20%">Gambar</th>
            <th width="20%">Title</th>
            <th width="20%">Caption</th>
            <th width="15%" class="text-center">A K S I</th>
        </thead>
        <tbody>
        @php
            $no=1;
        @endphp
        @foreach ($tbslide as $tbslide)
        <tr>
            <td>{{$no}}</td>
            <td><img src="{{asset('images/slide/'.$tbslide->foto_slide)}}" class="img img-responsive" width="50%" alt="" srcset=""></td>
            <td>{{$tbslide->title}}</td>
            <td>{{$tbslide->caption}}</td>
            <td>
                <a rel="tooltip" title="Edit" class="btn btn-primary btn-link btn-sm" href="{{url('/admin/slide/update-'.$tbslide->IdSlide)}}"><i class="material-icons" >edit</i></a>
                
                <button type="button" rel="tooltip" title="Delete" class="btn btn-danger btn-link btn-sm"><i class="material-icons" onclick="hapusSlide('{{url('/admin/slide/hapus-'.$tbslide->IdSlide)}}')">close</i></button>
            </td>
        </tr>
        @php
            $no++;
        @endphp
        @endforeach
    </tbody>
    </table>

    </div>
</div>
</div>

</div>
</div>


<!-- The Modal -->
<div class="modal" id="modalDelete">
        <div class="modal-dialog">
          <div class="modal-content">
      
            <!-- Modal Header -->
            <div class="modal-header">
              <h4 class="modal-title">Konfirmasi</h4>
              <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
      
            <!-- Modal body -->
            <div class="modal-body">
              Anda Yakin ?
            </div>
      
            <!-- Modal footer -->
            <div class="modal-footer">
              <a class="btn btn-primary" href="" id="btnYa">YA</a>
            </div>
      
          </div>
        </div>
      </div>
@endsection
@section('js')
<script>
    var sukses = 1;
    if(sukses = {{Session::get('status')}}){
        md.notif("top","right", "Berhasil ...", "info");
    }else{
        md.notif("top","right", "Gagal ...", "danger");
    }
</script>
<script>
$(document).ready(function(){
    $('#example').DataTable();
});

function hapusSlide(idSlide){
    $('#btnYa').attr("href", idSlide);
    $('#modalDelete').modal();
}
</script>
@endsection