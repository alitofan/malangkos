@extends('template')
@section('about','active')
@section('isimenu')
    <section class="banner-area organic-breadcrumb">
    <div class="container">
      <div class="breadcrumb-banner d-flex flex-wrap align-items-center justify-content-end">
        <div class="col-first">
          <h1>Tentang Kami</h1>
          <nav class="d-flex align-items-center">
            <a href="index.html">Home<span class="lnr lnr-arrow-right"></span></a>
            <a href="single-product.html">Tentang Kami</a>
          </nav>
        </div>
      </div>
    </div>
  </section>
@endsection