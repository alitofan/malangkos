@extends('template')
@section('beranda','active')
@section('isimenu')
  <section class="banner-area">
    <div class="container">
      <div class="row fullscreen align-items-center justify-content-start">
        <div class="col-lg-12">
          <div class="active-banner-slider owl-carousel">
            <!-- single-slide -->
            <div class="row single-slide align-items-center d-flex">
              <div class="col-lg-5 col-md-6">
                <div class="banner-content">
                  <h1>Malang <br>Kost!</h1>
                  <p>Malang kos. Malang kost adalah sebuah website penyedia layanan kost daerah malang. Dimana malang kos menyediakan tempat kost dengan harga terjangkau dan lebih mudah dalam mencari tempat kos.  </p>
                  <div class="add-bag d-flex align-items-center">
                 
                  </div>
                </div>
              </div>
              <div class="col-lg-7">
                <div class="banner-img">
                  <img class="img-fluid" style="width: 200px;" src="{{asset('frontend/img/banner/banner-imgg.png')}}" alt="">
                </div>
              </div>
            </div>
            <!-- single-slide -->
            <div class="row single-slide">
              <div class="col-lg-5">
                <div class="banner-content">
                  <h3>Lihat Beberapa Kos <br>Terupdate!</h3>
                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                    dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation.</p>
                  <div class="add-bag d-flex align-items-center">
                    <a class="add-btn" href=""><span class="lnr lnr-cross"></span></a>
                    <span class="add-text text-uppercase" ><a href="{{url('/berandakos')}}">Lihat Daftar</a></span>
                  </div>
                </div>
              </div>
              <div class="col-lg-7">
                <div class="banner-img">
                  <img class="img-fluid" src="img/banner/banner-img.png" alt="">
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- End banner Area -->

  <!-- start features Area -->
  <section class="features-area section_gap">
  
  </section>
  <!-- end features Area -->


    <!-- daftar rumah kos -->
  <section class="">
    <div class="single-product-slider">
      <div class="container">
        <div class="row justify-content-center">
          <div class="col-lg-6 text-center">
            <div class="section-title">
              <h1>Daftar Rumah Kost</h1>
              <p>Berikut ini list rumah kos yang sudah terdaftar di website Malangkos </p>
            </div>
          </div>
        </div>
        <div class="row">
          <!-- single product -->
          <div class="col-lg-3 col-md-6">
            <div class="single-product">
              <img class="img-fluid" src="{{asset('frontend/img/product/p1.jpg')}}" alt="">
              <div class="product-details">
                <h6>pemilik kos ibu nanik</h6>
                <div class="price">
                  <h6>Rp 350.00/bulan</h6>
                  <h6 class="l-through">tahun</h6>
                </div>
                 <div class="prd-bottom">

                  <a class="social-info">
                    <span class="ti-bag"></span>
                    <p class="hover-text">kusus putri</p>
                  </a>
                  <a class="social-info">
                    <span class="lnr lnr-heart"></span>
                    <p class="hover-text">kamar atas</p>
                  </a>
                  <a class="social-info">
                    <span class="lnr lnr-sync"></span>
                    <p class="hover-text">jl margan blog 2</p>
                  </a>
                  <a href="{{url('detailkos')}}" class="social-info">
                    <span class="lnr lnr-move"></span>
                    <p class="hover-text">Lihat Detail</p>
                  </a>
                </div>
              </div>
            </div>
          </div>
          <!-- single product -->
             <div class="col-lg-3 col-md-6">
            <div class="single-product">
              <img class="img-fluid" src="{{asset('frontend/img/product/p1.jpg')}}" alt="">
              <div class="product-details">
                <h6>pemilik kos ibu surti</h6>
                <div class="price">
                  <h6>Rp 450.00/bulan</h6>
                  <h6 class="l-through">tahun</h6>
                </div>
                <div class="prd-bottom">

                  <a class="social-info">
                    <span class="ti-bag"></span>
                    <p class="hover-text">kusus putri</p>
                  </a>
                  <a class="social-info">
                    <span class="lnr lnr-heart"></span>
                    <p class="hover-text">kamar atas</p>
                  </a>
                  <a class="social-info">
                    <span class="lnr lnr-sync"></span>
                    <p class="hover-text">jl jombang blog 2</p>
                  </a>
                  <a href="{{url('detailkos')}}" class="social-info">
                    <span class="lnr lnr-move"></span>
                    <p class="hover-text">lihat detail</p>
                  </a>
                </div>
              </div>
            </div>
          </div>
          <!-- single product -->
          <div class="col-lg-3 col-md-6">
            <div class="single-product">
              <img class="img-fluid" src="{{asset('frontend/img/product/p1.jpg')}}" alt="">
              <div class="product-details">
                <h6>pemilik kos ibu hani</h6>
                <div class="price">
                  <h6>Rp 400.00/bulan</h6>
                  <h6 class="l-through">tahun</h6>
                </div>
                <div class="prd-bottom">

                  <a class="social-info">
                    <span class="ti-bag"></span>
                    <p class="hover-text">kusus Putra</p>
                  </a>
                  <a class="social-info">
                    <span class="lnr lnr-heart"></span>
                    <p class="hover-text">kamar atas</p>
                  </a>
                  <a class="social-info">
                    <span class="lnr lnr-sync"></span>
                    <p class="hover-text">jl sudirman blog 2</p>
                  </a>
                  <a href="{{url('detailkos')}}" class="social-info">
                    <span class="lnr lnr-move"></span>
                    <p class="hover-text">lihat detail</p>
                  </a>
                </div>
              </div>
            </div>
          </div>
          <!-- single product -->
          <div class="col-lg-3 col-md-6">
            <div class="single-product">
              <img class="img-fluid" src="{{asset('frontend/img/product/p1.jpg')}}" alt="">
              <div class="product-details">
                <h6>pemilik kos ibu yuri</h6>
                <div class="price">
                  <h6>Rp 500.00/bulan</h6>
                  <h6 class="l-through">tahun</h6>
                </div>
                <div class="prd-bottom">

                  <a class="social-info">
                    <span class="ti-bag"></span>
                    <p class="hover-text">kusus putri</p>
                  </a>
                  <a class="social-info">
                    <span class="lnr lnr-heart"></span>
                    <p class="hover-text">kamar bawah</p>
                  </a>
                  <a class="social-info">
                    <span class="lnr lnr-sync"></span>
                    <p class="hover-text">jl jalan blog 2</p>
                  </a>
                  <a href="{{url('detailkos')}}" class="social-info">
                    <span class="lnr lnr-move"></span>
                    <p class="hover-text">lihat detail</p>
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- single product slide -->
    <div class="single-product-slider">
      <div class="container">
      </div>
    </div>
  </section>
    <!-- end -->


<section>
    <div class="single-product-slider">
      <div class="container">
        <div class="row">
          <!-- single product -->
             <div class="col-lg-12 ">
            <div class="single-product">
              <div class="product-details">
                <div class="prd-bottom">
 <h2>Lihat Lebih Banyak Daftar Kos</h2>
              <p>Berikut ini list rumah kos yang sudah terdaftar di website Malangkos </p>
                  <a href="{{url('/berandakos')}}" class="social-info">
                    <span class="lnr lnr-move"></span>
                    <p class="hover-text">lihat semua</p>
                  </a>
                </div>
              </div>
            </div>
          </div>
          <!-- single product -->


        </div>
      </div>
    </div>
    <!-- single product slide -->
    <div class="single-product-slider">
      <div class="container">
      </div>
    </div>
</section>





  <!-- Gallery -->
  <section class="category-area" style="margin-top: 100px;">
    <div class="container">
      <h2>Gallery</h2>
      <hr>

      <div class="row justify-content-center">
        
        <div class="col-lg-12 col-md-12">
          <div class="row">
            @foreach($galeri as $galeri)
            <div class="col-lg-3 col-md-4">
              <div class="single-deal">
                <div class="overlay"></div>
                <img class="img-fluid w-100" src="{{asset('/images/galeri/'.$galeri->foto_galery)}}" alt="">
                <a href="{{asset('/images/galeri/'.$galeri->foto_galery)}}" class="img-pop-up" target="_blank">
                  <div class="deal-details">
                    <h6 class="deal-title">Sneaker for Sports</h6>
                  </div>
                </a>
              </div>
            </div>
            @endforeach
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- End Gallery -->

  <!-- Start brand Area -->
  <section class="brand-area section_gap">
    <div class="container">
      <div class="row">
        <a class="col single-img" href="#">
          <img class="img-fluid d-block mx-auto" src="img/brand/1.png" alt="">
        </a>
        <a class="col single-img" href="#">
          <img class="img-fluid d-block mx-auto" src="img/brand/2.png" alt="">
        </a>
        <a class="col single-img" href="#">
          <img class="img-fluid d-block mx-auto" src="img/brand/3.png" alt="">
        </a>
        <a class="col single-img" href="#">
          <img class="img-fluid d-block mx-auto" src="img/brand/4.png" alt="">
        </a>
        <a class="col single-img" href="#">
          <img class="img-fluid d-block mx-auto" src="img/brand/5.png" alt="">
        </a>
      </div>
    </div>
  </section>
  <!-- End brand Area -->

  <!-- Start related-product Area -->
  <section class="related-product-area section_gap_bottom">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-lg-6 text-center">
          <div class="section-title">
            <h1>Our team</h1>
            <p>Berikut ini adalah daftar Team Jalan Prgramer Studio</p>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-9">
          <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-6 mb-20">
              <div class="single-related-product d-flex">
                <a href="#"><img src="{{asset('frontend/img/r2.jpg')}}" alt=""></a>
                <div class="desc">
                  <a href="#" class="title">Black lace Heels</a>
                  <div class="price">
                    <h6>$189.00</h6>
                    <h6 class="l-through">$210.00</h6>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-6 mb-20">
              <div class="single-related-product d-flex">
                <a href="#"><img src="{{asset('frontend/img/r3.jpg')}}" alt=""></a>
                <div class="desc">
                  <a href="#" class="title">Black lace Heels</a>
                  <div class="price">
                    <h6>$189.00</h6>
                    <h6 class="l-through">$210.00</h6>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-6 mb-20">
              <div class="single-related-product d-flex">
                <a href="#"><img src="{{asset('frontend/img/r5.jpg')}}" alt=""></a>
                <div class="desc">
                  <a href="#" class="title">Black lace Heels</a>
                  <div class="price">
                    <h6>$189.00</h6>
                    <h6 class="l-through">$210.00</h6>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-6 mb-20">
              <div class="single-related-product d-flex">
                <a href="#"><img src="{{asset('frontend/img/r2.jpg')}}" alt=""></a>
                <div class="desc">
                  <a href="#" class="title">Black lace Heels</a>
                  <div class="price">
                    <h6>$189.00</h6>
                    <h6 class="l-through">$210.00</h6>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-6 mb-20">
              <div class="single-related-product d-flex">
                <a href="#"><img src="{{asset('frontend/img/r3.jpg')}}" alt=""></a>
                <div class="desc">
                  <a href="#" class="title">Black lace Heels</a>
                  <div class="price">
                    <h6>$189.00</h6>
                    <h6 class="l-through">$210.00</h6>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-6 mb-20">
              <div class="single-related-product d-flex">
                <a href="#"><img src="{{asset('frontend/img/r3.jpg')}}" alt=""></a>
                <div class="desc">
                  <a href="#" class="title">Black lace Heels</a>
                  <div class="price">
                    <h6>$189.00</h6>
                    <h6 class="l-through">$210.00</h6>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-3">
          <div class="ctg-right">
            <a href="#" target="_blank">
              <img class="img-fluid d-block mx-auto" src="{{asset('frontend/img/category/c6.png')}}" alt="">
            </a>
          </div>
        </div>
      </div>
    </div>
  </section>
  @endsection