@extends('template')
@section('berandapemkos','active')
@section('isimenu')
  <section class="banner-area">
    <div class="container">
      <div class="row fullscreen align-items-center justify-content-start">
        <div class="col-lg-12">
          <div class="active-banner-slider owl-carousel">
            <!-- single-slide -->
            <div class="row single-slide align-items-center d-flex">
              <div class="col-lg-5 col-md-6">
                <div class="banner-content">
                  <h1>Malang <br>Kost!</h1>
                  <p>Malang kos. Malang kost adalah sebuah website penyedia layanan kost daerah malang. Dimana malang kos menyediakan tempat kost dengan harga terjangkau dan lebih mudah dalam mencari tempat kos.  </p>
                </div>
              </div>
              <div class="col-lg-7">
                <div class="banner-img">
                  <img class="img-fluid" style="width: 200px;" src="{{asset('frontend/img/banner/banner-imgg.png')}}" alt="">
                </div>
              </div>
            </div>
            <!-- single-slide -->
            <div class="row single-slide">
              <div class="col-lg-5">
                <div class="banner-content">
                  <h3>Lihat Beberapa Kos <br>Terupdate!</h3>
                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                    dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation.</p>
                </div>
              </div>
              <div class="col-lg-7">
                <div class="banner-img">
                  <img class="img-fluid" src="{{asset('img/banner/banner-img.png')}}" alt="">
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- End banner Area -->


<section style="padding-bottom: 42px;padding-top: 42px;">
  <div class="container">
    <div class="row">
      <div class="col-xl-3 col-lg-4 col-md-5">
        <div class="sidebar-categories">
          <div class="head">FILTER</div>
          <ul class="main-categories">
            <li class="main-nav-list"><a data-toggle="collapse" href="#fruitsVegetable" aria-expanded="false" aria-controls="fruitsVegetable"><span
                 class="lnr lnr-arrow-right"></span>Tempat<span class="number">(53)</span></a>
              <ul class="collapse" id="fruitsVegetable" data-toggle="collapse" aria-expanded="false" aria-controls="fruitsVegetable">
                <li class="main-nav-list child"><a href="#">Kamar Atas<span class="number">(13)</span></a></li>
                <li class="main-nav-list child"><a href="#">Kamar Bawah<span class="number">(09)</span></a></li>
              </ul>
            </li>

            <li class="main-nav-list"><a data-toggle="collapse" href="#meatFish" aria-expanded="false" aria-controls="meatFish"><span
                 class="lnr lnr-arrow-right"></span>Harga<span class="number">(53)</span></a>
              <ul class="collapse" id="meatFish" data-toggle="collapse" aria-expanded="false" aria-controls="meatFish">
                <li class="main-nav-list child"><a href="#"> Rp 300.000 - Rp 400.000<span class="number">(13)</span></a></li>
                <li class="main-nav-list child"><a href="#"> > Rp 400.000 - Rp 500.000<span class="number">(09)</span></a></li>
                <li class="main-nav-list child"><a href="#"> > Rp 500.000 - Rp 600.000<span class="number">(09)</span></a></li>
                <li class="main-nav-list child"><a href="#"> > Rp 600.000<span class="number">(09)</span></a></li>
              </ul>
            </li>
            <li class="main-nav-list"><a data-toggle="collapse" href="#cooking" aria-expanded="false" aria-controls="cooking"><span
                 class="lnr lnr-arrow-right"></span>kos Putra<span class="number">(53)</span></a>
              <ul class="collapse" id="cooking" data-toggle="collapse" aria-expanded="false" aria-controls="cooking">
                <li class="main-nav-list child"><a href="#">Ibu Jannah<span class="number">(13)</span></a></li>
                <li class="main-nav-list child"><a href="#">Ibu Surti<span class="number">(09)</span></a></li>
              </ul>
            </li>
            <li class="main-nav-list"><a data-toggle="collapse" href="#beverages" aria-expanded="false" aria-controls="beverages"><span
                 class="lnr lnr-arrow-right"></span>Kos Putri<span class="number">(24)</span></a>
              <ul class="collapse" id="beverages" data-toggle="collapse" aria-expanded="false" aria-controls="beverages">
                <li class="main-nav-list child"><a href="#">Ibu Sunarti<span class="number">(13)</span></a></li>
                <li class="main-nav-list child"><a href="#">Ibu Wani<span class="number">(09)</span></a></li>
              </ul>
            </li>
          </ul>
        </div>
      </div>
      <div class="col-xl-9 col-lg-8 col-md-7">
        <!-- Start Filter Bar -->
        <div class="filter-bar d-flex flex-wrap align-items-center">
          <div class="sorting">
            <select>
              <option value="1">Default sorting</option>
              <option value="2">Default sorting</option>
              <option value="3">Default sorting</option>
            </select>
          </div>
          <div class="sorting mr-auto">
            <select>
              <option value="1">Show 12</option>
              <option value="2">Show 3</option>
              <option value="3">Show 4</option>
            </select>
          </div>
          <div class="pagination">
            <a href="#" class="prev-arrow"><i class="fa fa-long-arrow-left" aria-hidden="true"></i></a>
            <a href="#" class="active">1</a>
            <a href="#">2</a>
            <a href="#">3</a>
            <a href="#" class="dot-dot"><i class="fa fa-ellipsis-h" aria-hidden="true"></i></a>
            <a href="#">6</a>
            <a href="#" class="next-arrow"><i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
          </div>
        </div>
        <!-- End Filter Bar -->
        <!-- Start Best Seller -->
        <section class="lattest-product-area pb-40 category-list">
          <div class="row">
            <!-- single product -->
            <div class="col-lg-4 col-md-6">
              <div class="single-product">
                <img class="img-fluid" src="{{asset('frontend/img/product/p1.jpg')}}" alt="">
                  <div class="product-details">
                    <h6>pemilik kos ibu nanik</h6>
                    <div class="price">
                      <h6>Rp 150.00/bulan</h6>
                      <h6 class="l-through">tahun</h6>
                    </div>
                    <div class="prd-bottom">

                      <a href="" class="social-info">
                        <span class="ti-bag"></span>
                        <p class="hover-text">kusus putri</p>
                      </a>
                      <a href="" class="social-info">
                        <span class="lnr lnr-heart"></span>
                        <p class="hover-text">kamar atas</p>
                      </a>
                      <a href="" class="social-info">
                        <span class="lnr lnr-sync"></span>
                        <p class="hover-text">jl margan blog 2</p>
                      </a>
                      <a href="" class="social-info">
                        <span class="lnr lnr-move"></span>
                        <p class="hover-text">lihat detail</p>
                      </a>
                    </div>
                  </div>
              </div>
            </div>
            <!-- single product -->
            <div class="col-lg-4 col-md-6">
              <div class="single-product">
                <img class="img-fluid" src="{{asset('frontend/img/product/p1.jpg')}}" alt="">
                  <div class="product-details">
                    <h6>pemilik kos ibu nanik</h6>
                    <div class="price">
                      <h6>Rp 150.00/bulan</h6>
                      <h6 class="l-through">tahun</h6>
                    </div>
                    <div class="prd-bottom">

                      <a href="" class="social-info">
                        <span class="ti-bag"></span>
                        <p class="hover-text">kusus putri</p>
                      </a>
                      <a href="" class="social-info">
                        <span class="lnr lnr-heart"></span>
                        <p class="hover-text">kamar atas</p>
                      </a>
                      <a href="" class="social-info">
                        <span class="lnr lnr-sync"></span>
                        <p class="hover-text">jl margan blog 2</p>
                      </a>
                      <a href="{{url('/detailkos')}}" class="social-info">
                        <span class="lnr lnr-move"></span>
                        <p class="hover-text">qqqqqqqqq</p>
                      </a>
                    </div>
                  </div>
              </div>
            </div>
            <!-- single product -->
            <div class="col-lg-4 col-md-6">
              <div class="single-product">
                <img class="img-fluid" src="{{asset('frontend/img/product/p1.jpg')}}" alt="">
                  <div class="product-details">
                    <h6>pemilik kos ibu nanik</h6>
                    <div class="price">
                      <h6>Rp 150.00/bulan</h6>
                      <h6 class="l-through">tahun</h6>
                    </div>
                    <div class="prd-bottom">

                      <a href="" class="social-info">
                        <span class="ti-bag"></span>
                        <p class="hover-text">kusus putri</p>
                      </a>
                      <a href="" class="social-info">
                        <span class="lnr lnr-heart"></span>
                        <p class="hover-text">kamar atas</p>
                      </a>
                      <a href="" class="social-info">
                        <span class="lnr lnr-sync"></span>
                        <p class="hover-text">jl margan blog 2</p>
                      </a>
                      <a href="" class="social-info">
                        <span class="lnr lnr-move"></span>
                        <p class="hover-text">lihat detail</p>
                      </a>
                    </div>
                  </div>
              </div>
            </div>
            <!-- single product -->
            <div class="col-lg-4 col-md-6">
              <div class="single-product">
                <img class="img-fluid" src="{{asset('frontend/img/product/p1.jpg')}}" alt="">
                  <div class="product-details">
                    <h6>pemilik kos ibu nanik</h6>
                    <div class="price">
                      <h6>Rp 150.00/bulan</h6>
                      <h6 class="l-through">tahun</h6>
                    </div>
                    <div class="prd-bottom">

                      <a href="" class="social-info">
                        <span class="ti-bag"></span>
                        <p class="hover-text">kusus putri</p>
                      </a>
                      <a href="" class="social-info">
                        <span class="lnr lnr-heart"></span>
                        <p class="hover-text">kamar atas</p>
                      </a>
                      <a href="" class="social-info">
                        <span class="lnr lnr-sync"></span>
                        <p class="hover-text">jl margan blog 2</p>
                      </a>
                      <a href="" class="social-info">
                        <span class="lnr lnr-move"></span>
                        <p class="hover-text">lihat detail</p>
                      </a>
                    </div>
                  </div>
              </div>
            </div>
            <!-- single product -->
            <div class="col-lg-4 col-md-6">
              <div class="single-product">
                <img class="img-fluid" src="{{asset('frontend/img/product/p1.jpg')}}" alt="">
                  <div class="product-details">
                    <h6>pemilik kos ibu nanik</h6>
                    <div class="price">
                      <h6>Rp 150.00/bulan</h6>
                      <h6 class="l-through">tahun</h6>
                    </div>
                    <div class="prd-bottom">

                      <a href="" class="social-info">
                        <span class="ti-bag"></span>
                        <p class="hover-text">kusus putri</p>
                      </a>
                      <a href="" class="social-info">
                        <span class="lnr lnr-heart"></span>
                        <p class="hover-text">kamar atas</p>
                      </a>
                      <a href="" class="social-info">
                        <span class="lnr lnr-sync"></span>
                        <p class="hover-text">jl margan blog 2</p>
                      </a>
                      <a href="" class="social-info">
                        <span class="lnr lnr-move"></span>
                        <p class="hover-text">lihat detail</p>
                      </a>
                    </div>
                  </div>
              </div>
            </div>
            <!-- single product -->
            <div class="col-lg-4 col-md-6">
              <div class="single-product">
                <img class="img-fluid" src="{{asset('frontend/img/product/p1.jpg')}}" alt="">
                  <div class="product-details">
                    <h6>pemilik kos ibu nanik</h6>
                    <div class="price">
                      <h6>Rp 150.00/bulan</h6>
                      <h6 class="l-through">tahun</h6>
                    </div>
                    <div class="prd-bottom">

                      <a href="" class="social-info">
                        <span class="ti-bag"></span>
                        <p class="hover-text">kusus putri</p>
                      </a>
                      <a href="" class="social-info">
                        <span class="lnr lnr-heart"></span>
                        <p class="hover-text">kamar atas</p>
                      </a>
                      <a href="" class="social-info">
                        <span class="lnr lnr-sync"></span>
                        <p class="hover-text">jl margan blog 2</p>
                      </a>
                      <a href="" class="social-info">
                        <span class="lnr lnr-move"></span>
                        <p class="hover-text">lihat detail</p>
                      </a>
                    </div>
                  </div>
              </div>
            </div>
        </section>
        <!-- End Best Seller -->
        <!-- Start Filter Bar -->
        <div class="filter-bar d-flex flex-wrap align-items-center">
          <div class="sorting mr-auto">
            <select>
              <option value="1">Show 12</option>
              <option value="1">Show 12</option>
              <option value="1">Show 12</option>
            </select>
          </div>
          <div class="pagination">
            <a href="#" class="prev-arrow"><i class="fa fa-long-arrow-left" aria-hidden="true"></i></a>
            <a href="#" class="active">1</a>
            <a href="#">2</a>
            <a href="#">3</a>
            <a href="#" class="dot-dot"><i class="fa fa-ellipsis-h" aria-hidden="true"></i></a>
            <a href="#">6</a>
            <a href="#" class="next-arrow"><i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
          </div>
        </div>
        <!-- End Filter Bar -->
      </div>
    </div>
  </div>
</section>
  @endsection