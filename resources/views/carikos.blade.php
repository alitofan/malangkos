<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{asset('frontend/bootstrap-4.0.0/asset/css/bootstrap.min.css')}}" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="{{asset('frontend/bootstrap-4.0.0/asset/css/style.css')}}">
    <link href="https://fonts.googleapis.com/css?family=Viga" rel="stylesheet"> 
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">

    <title>Malang Kos</title>

<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!------ Include the above in your HEAD tag ---------->

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.3.3/css/swiper.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.3.3/js/swiper.min.js"></script>

<style type="text/css">
      .divider {border: 1px solid #ccc;}
img {width:100%;}

     .swiper-container {
      width: 100%;
      height: 100%;
    }
    .swiper-slide {
      text-align: center;
      font-size: 18px;
      background: #fff;
      /* Center slide text vertically */
      display: -webkit-box;
      display: -ms-flexbox;
      display: -webkit-flex;
      display: flex;
      -webkit-box-pack: center;
      -ms-flex-pack: center;
      -webkit-justify-content: center;
      justify-content: center;
      -webkit-box-align: center;
      -ms-flex-align: center;
      -webkit-align-items: center;
      align-items: center;
    }
</style>

<!-- bb -->
<style type="text/css">
  @media ( min-width: 768px ) {
    .grid-divider {
        position: relative;
        padding: 0;
    }
    .grid-divider>[class*='col-'] {
        position: static;
    }
    .grid-divider>[class*='col-']:nth-child(n+2):before {
        content: "";
        border-left: 1px solid #DDD;
        position: absolute;
        top: 0;
        bottom: 0;
    }
    .col-padding {
        padding: 0 15px;
    }
}
</style>



  </head>
  <body>

<!-- navbar -->
<nav class="navbar navbar-expand-lg navbar-light" style="color: white">
  <div class="container">
    <a class="navbar-brand" href="#">Malang Kos</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
      <div class="navbar-nav ml-auto">
        <a class="nav-item nav-link active" href="#">Beranda</a>
        <a class="nav-item nav-link" href="#">About</a>
        <a class="nav-item nav-link" href="#">Cari Kos</a>
        <a class="nav-item nav-link" href="#">Login</a>
        <a class="nav-item nav-link" href="#">Sign up</a>
      </div>
    </div>
  </div>
</nav>
<!-- endnavbar -->

<!-- list kos -->
  <div class="container" style="margin-bottom: 120px;margin-top: 40px;">
    <div class="row grid-divider">
    <div class="col-sm-8">
      <div class="col-padding">
        <h3 style="text-align: center;margin-bottom: 40px;">" Cari Kos yang Anda Minati "</h3>

<!-- list daftar kos -->

      <div class="row" style="margin-bottom: 10px;">

          <div class="col-sm-12">
            <div class="card-img"><img src="{{asset('frontend/img/slider/bg4.jpg')}}"></div>
        <!--   </div>

          <div class="col-sm-6"> -->
            <div class="card-body" style="margin-top: -10px;">
              <p style="font-size: 16px;color: #008080;">Pemilik : <span style="font-family: bold;">IBU SURTI</span>  </p>
             
             <div class="row" style="margin-top:-18px; ">
                <div class="col card-header text-white" style="text-align: left;margin-right: 30px;text-align: center;background-color:#008080;font-size: 10px;font-weight: bold;">
                  <i class="fa fa-envelope">KAMAR ATAS</i>
                </div>
                <div class="col card-header text-white" style="text-align: left;margin-left: 26px;text-align: center;background-color: #008080;font-size: 10px;font-weight: bold;">
                  <i class="fa fa-envelope">KOS PUTRI</i>
                </div>
              </div>

              <p style="margin-top: 4px;">Harga  : <span>Rp 400.000,</span></p>
              <p  style="margin-top: -18px;margin-bottom: 4px;">Alamat : <span>Jl. Sentana</span></p>
              <button type= "button" class="btn btn-outline-warning btn-block btn-sm">Read more</button>
            </div>
          </div>

      </div>




<!-- end list daftar kos -->
        
      </div>
    </div>

<!-- filter -->
    <div class="col-sm-4">
      <div class="col-padding">
        <h3 style="text-align: center;margin-bottom: 40px;">Cari Berdasarkan</h3>
        <div class="col card-header text-white" style="text-align: left;margin-right: 12px;background-color: #20B2AA;font-size: 16px;font-weight: bold;">
            <i class="fa fa-envelope">FASILITAS</i>
        </div>
        <ul class="list-group">
          <li class="list-group-item">Kamar Bawah</li>
          <li class="list-group-item">Kamar Atas</li>
        </ul>

        <br>

        <div class="col card-header text-white" style="text-align: left;margin-right: 12px;background-color: #20B2AA;font-size: 16px;font-weight: bold;">
            <i class="fa fa-envelope">HARGA</i>
        </div>
        <ul class="list-group">
          <li class="list-group-item">Rp 300.000 - Rp 400.000</li>
          <li class="list-group-item">> Rp 400.000 - Rp 500.000</li>
          <li class="list-group-item">> Rp 500.000 - Rp 600.000</li>
          <li class="list-group-item">> Rp 700.000</li>
        </ul>

        <br>

        <div class="col card-header text-white" style="text-align: left;margin-right: 12px;background-color: #20B2AA;font-size: 16px;font-weight: bold;">
            <i class="fa fa-envelope">KOS PUTRA</i>
        </div>
        <ul class="list-group">
          <li class="list-group-item">Ibu Jannah</li>
          <li class="list-group-item">Ibu Sri</li>
        </ul>

        <br>

        <div class="col card-header text-white" style="text-align: left;margin-right: 12px;background-color: #20B2AA;font-size: 16px;font-weight: bold;">
            <i class="fa fa-envelope">KOS PUTRI</i>
        </div>
        <ul class="list-group">
          <li class="list-group-item">Ibu Jannah</li>
          <li class="list-group-item">Ibu Sri</li>
        </ul>

      </div>
    </div> 
<!-- end filter -->
    </div>
</div>
<!-- end list kos -->

<!-- galery -->
  <div class="container">
      <div class="row text-center mb-3">
          <div class="col-md-12">
              <h2>Top List of Boarding Places</h2>
              <hr>
          </div>
      </div>
    <div class="row">
      <!-- Swiper -->
      <div class="swiper-container">
        <div class="swiper-wrapper">

          <div class="swiper-slide">
            <div class="row">
              <div class="col-md-4">
                <div class="card">
                  <div class="card-img"  ><img style="height: 305px;width: 100%; " src="{{asset('frontend/img/slider/bg1.jpg')}}"></div>
                </div>
              </div>
            </div>
          </div>

          <div class="swiper-slide">
            <div class="row">
              <div class="col-md-4">
                <div class="card">
                  <div class="card-img"  ><img style="height: 305px;width: 100% " src="{{asset('frontend/img/slider/bg4.jpg')}}"></div>
                </div>
              </div>
            </div>
          </div>
            
        </div>
        <!-- Add Pagination -->
        <div class="swiper-pagination"></div>
        <!-- Add Arrows -->
        <div class="swiper-button-next"></div>
        <div class="swiper-button-prev"></div>
      </div>

    </div>
    <hr>
  </div>
<!-- end galery -->

<!-- footer -->
<footer class="mainfooter" role="contentinfo">
<div class="footer-middle">
  <div class="container">
    <div class="row">
      <div class="col-md-3 col-sm-6">
        <!--Column1-->
        <div class="footer-pad">
          <h4>Malang Kos</h4>
          <address>
            <p>Website penyedia informasi kos sekitar malang</p>
              </address>
        </div>
      </div>
      <div class="col-md-3 col-sm-6">
      </div>
      <div class="col-md-3 col-sm-6">
        <!--Column1-->
      </div>
      <div class="col-md-3 col-sm-6">
        <!--Column1-->
        <div class="footer-pad">
          <h4>Contact us </h4>
          <ul class="list-unstyled" style="list-style-type: square;">
              <li>Indonesia</li>
              <li>jalanprogramer@gmail.com</li>
              <li>081789234678</li>
          </ul>
        </div>
      </div>
    </div>
  </div>
  </div>

  <div class="footer-bottom" style="text-align: center;">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <!--Footer Bottom-->
          <p>&copy; Copyright 2019 | Design With by Jalanprogramer studio.</p>
          <img src="{{asset('frontend/img/icons/instagram.png')}}" style="width: 3%">
          <img src="{{asset('frontend/img/icons/fb.png')}}" style="width: 5%">
              <img src="{{asset('frontend/img/icons/wa.png')}}" style="width: 3%">
        </div>
      </div>
    </div>
  </div>
</footer>
<!-- end footer -->





    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="{{asset('frontend/bootstrap-4.0.0/asset/js/jquery-3.2.1.slim.min.js')}}" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="{{asset('frontend/bootstrap-4.0.0/asset/js/popper.min.js')}}" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="{{asset('frontend/bootstrap-4.0.0/asset/js/bootstrap.min.js')}}" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

    <script type="text/javascript">
       var swiper = new Swiper('.swiper-container', {
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },
    });
    </script>
  </body>
</html>