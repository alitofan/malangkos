@extends('template')
@section('detailkos','active')
@section('isimenu')
  <section class="banner-area">
    <div class="container">
      <div class="row fullscreen align-items-center justify-content-start">
        <div class="col-lg-12">
          <div class="active-banner-slider owl-carousel">
            <!-- single-slide -->
            <div class="row single-slide align-items-center d-flex">
              <div class="col-lg-5 col-md-6">
                <div class="banner-content">
                  <h1>Malang <br>Kost!</h1>
                  <p>Malang kos. Malang kost adalah sebuah website penyedia layanan kost daerah malang. Dimana malang kos menyediakan tempat kost dengan harga terjangkau dan lebih mudah dalam mencari tempat kos.  </p>
                </div>
              </div>
              <div class="col-lg-7">
                <div class="banner-img">
                  <img class="img-fluid" style="width: 200px;" src="{{asset('frontend/img/banner/banner-imgg.png')}}" alt="">
                </div>
              </div>
            </div>
            <!-- single-slide -->
            <div class="row single-slide">
              <div class="col-lg-5">
                <div class="banner-content">
                  <h3>Lihat Beberapa Kos <br>Terupdate!</h3>
                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                    dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation.</p>
                </div>
              </div>
              <div class="col-lg-7">
                <div class="banner-img">
                  <img class="img-fluid" src="img/banner/banner-img.png" alt="">
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- End banner Area -->


 <!--================Blog Area =================-->
    <section class="blog_area single-post-area section_gap">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 posts-list">
                    <div class="single-post row">
                        <div class="col-lg-12">
                            <div class="feature-img">
                                <img class="img-fluid" src="{{asset('frontend/img/blog/feature-img1.jpg')}}" alt="">
                            </div>
                        </div>
                        <div class="col-lg-3  col-md-3">
                            <div class="blog_info text-right">
                                <div class="post_tag">
                                    <a>Kterangan untuk</a> <br>
                                    <a class="active" style="font-size: 20px;">Kamar ini</a>
<!--                                     <a href="#">Politics,</a>
                                    <a href="#">Lifestyle</a>
 -->                                </div>
                                <ul class="blog_meta list">
                                    <li><a>Kusus Putra<i class="lnr lnr-user"></i></a></li>
                                    <li><a>Rp 400.000/Bulan<i class="lnr lnr-calendar-full"></i></a></li>
                                    <li style="font-style: italic;"><a >Jl. Margan blog 2 gang 3</a></li>
                                    <li><a>0897847<i class="lnr lnr-bubble"></i></a></li>
                                    <li><a>Tersedia<i class="lnr lnr-bubble"></i></a></li>
                                </ul>
                                <ul class="social-links">
                                    <li><a><i class="fa fa-facebook"></i></a></li>
                                    <li><a><i class="fa fa-twitter"></i></a></li>
                                    <li><a><i class="fa fa-github"></i></a></li>
                                    <li><a><i class="fa fa-behance"></i></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-lg-9 col-md-9 blog_details">
                            <h2>Keterangan untuk kos di temat ini </h2>
                            <p class="excert">
                                Kamar hanya kusus untuk putra dengan harga Rp 400.000/bulan
                            </p>
                            <p>
                                Fasilitas seperti wifi dan air listrik di tanggung oleh pemilik kos dengan ukuran listrik 5watt untuk kamar ini
                            </p>
                            <p>
                                Peraturan kusus yang wajib anda ketahui bila ingin memesannya <br>
                                1. Setelah mandi harus buka pintunya bro
                            </p>
                        </div>
                    </div>
                    <div class="navigation-area">
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-12 nav-left flex-row d-flex justify-content-start align-items-center">
                                <div class="arrow">
                                    <a href="#"><span class="lnr text-white lnr-arrow-left"></span></a>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-12 nav-right flex-row d-flex justify-content-end align-items-center">

                                <div class="arrow">
                                    <a href="#"><span class="lnr text-white lnr-arrow-right"></span></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
                <div class="col-lg-4">
                    <div class="blog_right_sidebar">
                        <aside class="single_sidebar_widget search_widget">
                          <h3 class="widget_title">Pemilik Kos</h3>
                            <div class="br"></div>
                        </aside>
                        <aside class="single_sidebar_widget author_widget">
                            <img class="author_img rounded-circle" src="{{asset('frontend/img/blog/author.png')}}" alt="">
                            <h4>Ibu Sunarti Kusuma</h4>
                            <p>Umur 50 tahun</p>
                           <!--  <div class="social_icon">
                                <a href="#"><i class="fa fa-facebook"></i></a>
                                <a href="#"><i class="fa fa-twitter"></i></a>
                                <a href="#"><i class="fa fa-github"></i></a>
                                <a href="#"><i class="fa fa-behance"></i></a>
                            </div> -->
                            <p>terimakasih atas kunjungannya, dan terimakasih atas kepercayaannya untuk memesan kamar di tempat saya semoga betah.</p>
                        </aside>
                        <aside class="single_sidebar_widget search_widget">
                          <h3 class="widget_title">Lihat Semua Kamar</h3>
                        </aside>                
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--================Blog Area =================-->




  @endsection