<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class tbkamar extends Model
{
    //
    protected $table = "tbkamar";
    protected $guarded = [];
    public $timestamps = false;

    public function get_pemilik(){
        return $this->belongsTo('App\Models\tbpemilikkos', "IdPemilik_kos","IdPemilik_kos");
    }
}
