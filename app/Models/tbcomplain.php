<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class tbcomplain extends Model
{
    //
    protected $table = "tbcomplain";
    protected $guarded = [];
    public $timestamps = false;
}
