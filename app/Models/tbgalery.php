<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class tbgalery extends Model
{
    //
    protected $table = "tbgalery";
    protected $guarded = [];
    public $timestamps = false;
}
