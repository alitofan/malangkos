<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class tbadmin extends Model
{
    //

    protected $table = "tbadmin";
    protected $guarded = [];
    public $timestamps = false;
}
