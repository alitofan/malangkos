<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class tbslide extends Model
{
    //
    protected $table = "tbslide";
    protected $guarded = [];
    public $timestamps = false;
}
