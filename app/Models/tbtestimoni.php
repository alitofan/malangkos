<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class tbtestimoni extends Model
{
    //
    protected $table = "tbtestimoni";
    protected $guarded = [];
    public $timestamps = false;
}
