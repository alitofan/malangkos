<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class tbpemilikkos extends Model
{
    //
    protected $table = "tbpemilikkos";
    protected $guarded = [];
    public $timestamps = false;

    public function get_kamar(){
        return $this->hasMany("App\Models\tbkamar");
    }
}
