<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

// optional
use Exception;
use File;

// Models
use App\Models\tbkamar;
use App\Models\tbpemilikkos;

class datakamarController extends Controller
{
    //

    public function frmtambah(tbkamar $tbkamar, tbpemilikkos $tbpemilik){
        $return = [
            "pemilik"   => $tbpemilik->all() 
        ];
        return view('Backend/datakamar/datakamarF', $return);
    }

    public function insert(Request $req, tbkamar $tbkamar, tbpemilikkos $tbpemilik, $status = 0, $msg = "Not Worked"){
        $req->validate([
            "nmKamar"           => "required",
            "hargaKamar"        => "required",
            "fasilitasKamar"    => "required",
            "ukuranKamar"       => "required",
            "nmPemilik"         => "required",
            "fotoKamar"         => "required|image"
        ],[
            "required"          => "Tidak Boleh Kosong !",
            "image"             => "Gambar Saja !"
        ]);

        try {
            $fotoKamar = $req->file('fotoKamar');
            $exfile = $fotoKamar->getClientOriginalExtension();
            $imgName = date("Ymdhis").".".$exfile;
            $req->file('fotoKamar')->move('images/kamar', $imgName);

            $tbkamar->create([
                "nm_kamar"      => $req->nmKamar,
                "harga"         => $req->hargaKamar,
                "fasilitas"     => $req->fasilitasKamar,
                "ukuran"        => $req->ukuranKamar,
                "IdPemilik_kos" => $req->nmPemilik,
                "status"        => 1,
                "foto_kamar"    => $imgName
            ]);
            $status = 1;
            $msg = "Berhasil !";
        } catch (Exception $e) {
            $status = 2;
            $msg = "Gagal !";
        }
        $return = [
            "status"    => $status,
            "msg"       => $msg
        ];
        return redirect()->back()->with($return);
    }

    public function perUpdate($id, tbkamar $tbkamar, tbpemilikkos $tbpemilik, $status = 0, $msg = "Not Worked"){
        
        $return = [
            "tbkamar"       => $tbkamar->with("get_pemilik")->where("IdKamar", $id)->first(),
            "tbpemilik"     => $tbpemilik->all()
        ];
        return view("Backend/datakamar/datakamarF2", $return);
    }

    public function update1(Request $req, tbkamar $tbkamar, $status = 0, $msg = "Not Worked"){
        $req->validate([
            "nmKamar"           => "required",
            "hargaKamar"        => "required",
            "fasilitasKamar"    => "required",
            "ukuranKamar"       => "required",
            "nmPemilik"         => "required",
        ],[
            "required"          => "Tidak Boleh Kosong !"
        ]);
        try {
            $tbkamar->where("IdKamar", $req->idKamar)->update([
                "nm_kamar"      => $req->nmKamar,
                "harga"         => $req->hargaKamar,
                "fasilitas"     => $req->fasilitasKamar,
                "ukuran"        => $req->ukuranKamar,
                "IdPemilik_kos" => $req->nmPemilik,
                "status"        => $req->statusKamar,
            ]);
            $status = 1;
            $msg    = "Berhasil !";
        } catch (Exception $e) {
            $status = 2;
            $msg    = "Gagal !".$e;
        }
        $return = [
            "status"    => $status,
            "msg"       => $msg
        ];
        return redirect()->back()->with($return);
    }

    public function update2(Request $req, tbkamar $tbkamar, $status = 0, $msg = "Not Worked"){
        $req->validate([
            "fotoKamar" => "required|image"
        ],[
            "required"  => "Tidak Boleh Kosong !",
            "image"     => "Gambar Saja !"
        ]);
        try {
            $fotoKamar = $req->file('fotoKamar');
            $exfile = $fotoKamar->getClientOriginalExtension();
            $imgName = date("Ymdhis").".".$exfile;
            $req->file('fotoKamar')->move('images/kamar', $imgName);

            $tbkamar->where("IdKamar", $req->idKamar)->update([
                "foto_kamar"    => $imgName
            ]);

            if(File::exists(public_path("images/kamar/".$req->oldFotoKamar))){
                File::delete(public_path("images/kamar/".$req->oldFotoKamar));
            }

            $status = 1;
            $msg    = "Berhasil !";
        } catch (Exception $e) {
            $status = 2;
            $msg    = "Gagal !".$e;
        }
        $return = [
            "status"    => $status,
            "msg"       => $msg
        ];
        return redirect()->back()->with($return);
    }

    public function hapus($id, tbkamar $tbkamar, $status = 0, $msg = "Not Worked!"){
        try {
            $dKamar = $tbkamar->where("IdKamar", $id)->first();
            if(File::exists(public_path("images/kamar/".$dKamar->foto_kamar))){
                File::delete(public_path("images/kamar/".$dKamar->foto_kamar));
            }
            $tbkamar->where("IdKamar", $id)->delete();
            $status = 1;
            $msg    = "Berhasil !";
        } catch (Exception $e) {
            $status = 2;
            $msg    = "Gagal !".$e;
        }
        $return = [
            "status"    => $status,
            "msg"       => $msg
        ];
        return redirect()->back()->with($return);
    }
}
