<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\tbpemilikkos;
use App\Models\tbkamar;
use App\Models\tbgalery;
use App\Models\tbadmin;
use App\Models\tbslide;


class frontendController extends Controller
{
    public function TampilAll(){
    	$data = [
    		'galeri' => tbgalery::all(),
    	];
    	return view('beranda', $data);
    }
}
