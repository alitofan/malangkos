<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Exception;
use File;
 
use App\Models\tbgalery;

class galeriController extends Controller
{
    public function tambah(){
        return view("Backend/galeri/galeriF");
    }
     public function simpan(Request $req, tbgalery $tbgalery, $status = 0, $msg = "Not Worked"){
        $req->validate([
            "gbrGaleri"      => "required|image",
            "gbrHasilCrop"  => "required"
        ],[
            "required"      => "Tidak boleh kosong",
            "image"         => "Hanya menerima inputan berupa file gambar"
        ]);
        try {
            $image_array_1 = explode(";", $req->gbrHasilCrop);
            $image_array_2 = explode(",", $image_array_1[1]);
            $data = base64_decode($image_array_2[1]);
            $imgName= date("Ymdhis").'.png';
            $dest = public_path("images/galeri/".$imgName);
            file_put_contents($dest, $data);

            $tbgalery->create([
                "foto_galery"    => $imgName
            ]);
            $status = 1;
            $msg = "Berhasil";
        } catch (Exception $e) {
            $status = 2;
            $msg = "Gagal ". $e;
        }
        $return = [
            "status"    => $status,
            "msg"       => $msg
        ];
        return redirect()->back()->with($return);
        // return "hehe";
    }

    public function perupdate ($id, tbgalery $tbgalery){
      $return = [
            "tbgalery"   => $tbgalery->where("IdGalery", $id)->first(),
        ];
        return view("Backend/galeri/galeriF2", $return);
    }

    public function update1(Request $req, tbgalery $tbgalery, $status = 0, $mgs = "Not Worked"){
        $dGaleri = $tbgalery->where("IdGalery", $req->idGaleri)->first();
        try {
            $image_array_1 = explode(";", $req->gbrHasilCrop);
            $image_array_2 = explode(",", $image_array_1[1]);
            $data = base64_decode($image_array_2[1]);
            $imgName= date("Ymdhis").'.png';
            $dest = public_path("images/galeri/".$imgName);
            file_put_contents($dest, $data);

            if(File::exists(public_path('images/galeri/'.$dGaleri->foto_galery))){
                File::delete(public_path('images/galeri/'.$dGaleri->foto_galery));
            }

            $tbgalery->where("IdGalery", $req->idGaleri)->update([
                "foto_galery"    => $imgName
            ]);

            $status = 1;
            $msg = "Berhasil";
        } catch (Exception $e) {
            $status = 2;
            $msg = "Gagal ". $e;
        }
        $return = [
            "status"    => $status,
            "msg"       => $msg
        ];
        return redirect()->back()->with($return);
    }

    public function hapus($id, tbgalery $tbgalery, $status = 0, $mgs = "Not Worked"){
        $dGaleri = $tbgalery->where('IdGalery', $id)->first();
        try {
            if(File::exists(public_path('images/galeri/'.$dGaleri->foto_galery))){
            File::delete(public_path('images/galeri/'.$dGaleri->foto_galery));
            }
            $tbgalery->where("IdGalery", $id)->delete();
            $status = 1;
            $msg = "Berhasil";
        } catch (Exception $e) {
            $status = 2;
            $msg = "Gagal ".$e;
        }
        $return = [
            "status"    => $status,
            "msg"       => $msg
        ];
        return redirect()->back()->with($return);
    }

    }