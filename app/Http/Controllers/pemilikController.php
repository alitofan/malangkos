<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Exception;
use File;

use App\Models\tbpemilikkos;

class pemilikController extends Controller
{
    //
    public function tambahpemilikkos(){
        return view("Backend/pemilikkos/pemilikkosF");
    }

    public function simpanpemilikkos(Request $req, tbpemilikkos $pk, $status = 0, $pesan = "Terjadi Kesalahan"){
        $req->validate([
            "nmPemilikkos"  => "required",
            "alamatPemilikkos"  => "required",
            "telpPemilikkos"    => "required|numeric",
            "waPemilikkos"      => "required|numeric",
            "ketPemilikkos"     => "required",
            "status"            => "required",
            "fotoKtp"          => "required|image",
            "fotoPerjanjian"   => "required|image"
        ],[
            "required"          => "Tidak boleh kosong",
            "image"             => "Gambar saja",
            "numeric"           => "Angka saja"
        ]);

        try {
            //code...
            $fotoKtp = $req->file('fotoKtp');
            $exfile = $fotoKtp->getClientOriginalExtension();
            $ktpName = date("Ymdhis").".".$exfile;
            $req->file('fotoKtp')->move('images/pemilikkos', $ktpName);

            $fotoPerjanjian = $req->file('fotoPerjanjian');
            $exfile = $fotoPerjanjian->getClientOriginalExtension();
            $perjanjianName = date("Ymdhis")."2.".$exfile;
            $req->file('fotoPerjanjian')->move('images/pemilikkos', $perjanjianName);

            $insert = $pk->create([
                "nm_Pemilikkos"     => $req->nmPemilikkos,
                "alamat"            => $req->alamatPemilikkos,
                "no_telp"           => $req->telpPemilikkos,
                "no_wa"             => $req->waPemilikkos,
                "ket_pemilik"       => $req->ketPemilikkos,
                "status"            => $req->status,
                "Idadmin"           => "101",
                "foto_ktp"          => $ktpName,
                "foto_perjanjian"   => $perjanjianName
            ]);
            
            $status = 1;
            $pesan = "Data berhasil dismpan";
        } catch (Exception $e) {
            //throw $th;
            $status = 2;
            $pesan = "Terjadi Kesalahan ". $e;
        }
        $return = [
            'status'    => $status,
            'pesan'     => $pesan
        ];
        return redirect()->back()->with($return);
    }

    public function perUpdate($idPemilik, tbpemilikkos $pk){
        $dPemilik = $pk->where("IdPemilik_kos", $idPemilik)->first();
        $return = [
            'pemilik'   => $dPemilik
        ];
        return view("Backend/pemilikkos/pemilikkosF2", $return);
    }

    public function update1(Request $req, tbpemilikkos $pk, $status = 0, $pesan = "Not Worked"){
        $req->validate([
            "nmPemilikkos"      => "required",
            "alamatPemilikkos"  => "required",
            "telpPemilikkos"    => "required|numeric",
            "waPemilikkos"      => "required|numeric",
            "ketPemilikkos"     => "required",
            "status"            => "required",
        ],[
            "required"          => "Tidak boleh kosong",
            "numeric"           => "Angka saja"
        ]);

        try {
            $update = $pk->where("IdPemilik_kos", $req->IdPemilik)->update([
                "nm_Pemilikkos"     => $req->nmPemilikkos,
                "alamat"            => $req->alamatPemilikkos,
                "no_telp"           => $req->telpPemilikkos,
                "no_wa"             => $req->waPemilikkos,
                "ket_pemilik"       => $req->ketPemilikkos,
                "status"            => $req->status,
                "Idadmin"           => "101",
            ]);
            $status = 1;
            $pesan = "Data berhasil dismpan";
        } catch (Exception $e) {
            //throw $th;
            $status = 2;
            $pesan = "Terjadi Kesalahan ". $e;
        }

        $return = [
            'status'    => $status,
            'pesan'     => $pesan
        ];
        return redirect()->back()->with($return);
    }
    public function update2(Request $req, tbpemilikkos $pk, $status = 0, $pesan = "Not Worked"){
        $req->validate([
            "fotoKtp"          => "required|image",
        ],[
            "required"          => "Tidak boleh kosong",
            "image"             => "Gambar saja",
        ]);
        
        try {
            //code...
            $fotoKtp = $req->file('fotoKtp');
            $exfile = $fotoKtp->getClientOriginalExtension();
            $ktpName = date("Ymdhis").".".$exfile;
            $req->file('fotoKtp')->move('images/pemilikkos', $ktpName);
            $dPemilik =$pk->where("IdPemilik_kos", $req->IdPemilik)->first();
            
            if(File::exists(public_path("/images/pemilikkos/".$dPemilik->foto_ktp))){
                File::delete(public_path("/images/pemilikkos/".$dPemilik->foto_ktp));
            }
            $update = $pk->where("IdPemilik_kos", $req->IdPemilik)->update([
                "foto_ktp"           => $ktpName,
            ]);

            $status = 1;
            $pesan = "Data berhasil dismpan";
        } catch (Exception $e) {
            //throw $th;
            $status = 2;
            $pesan = "Terjadi Kesalahan ". $e;
        }

        $return = [
            'status'    => $status,
            'pesan'     => $pesan
        ];
        return redirect()->back()->with($return);
    }

    public function update3(Request $req, tbpemilikkos $pk, $status = 0, $pesan = "Not Worked"){
        $req->validate([
            "fotoPerjanjian"          => "required|image",
        ],[
            "required"          => "Tidak boleh kosong",
            "image"             => "Gambar saja",
        ]);
        
        try {
            //code...
            $fotoPerjanjian = $req->file('fotoPerjanjian');
            $exfile = $fotoPerjanjian->getClientOriginalExtension();
            $perjanjianName = date("Ymdhis")."2.".$exfile;
            $req->file('fotoPerjanjian')->move('images/pemilikkos', $perjanjianName);
            $dPemilik =$pk->where("IdPemilik_kos", $req->IdPemilik)->first();
            
            if(File::exists(public_path("/images/pemilikkos/".$dPemilik->foto_perjanjian))){
                File::delete(public_path("/images/pemilikkos/".$dPemilik->foto_perjanjian));
            }
            $update = $pk->where("IdPemilik_kos", $req->IdPemilik)->update([
                "foto_perjanjian"           => $perjanjianName,
            ]);
            
            $status = 1;
            $pesan = "Data berhasil dismpan";
        } catch (Exception $e) {
            //throw $th;
            $status = 2;
            $pesan = "Terjadi Kesalahan ". $e;
        }

        $return = [
            'status'    => $status,
            'pesan'     => $pesan
        ];
        return redirect()->back()->with($return);
    }

    public function hapus($id, tbpemilikkos $pk, $status = 0, $pesan = "Not Worked"){
        try {
            $dPemilik =$pk->where("IdPemilik_kos", $id)->first();
            if(File::exists(public_path("/images/pemilikkos/".$dPemilik->foto_perjanjian))){
                File::delete(public_path("/images/pemilikkos/".$dPemilik->foto_perjanjian));
            }
            if(File::exists(public_path("/images/pemilikkos/".$dPemilik->foto_ktp))){
                File::delete(public_path("/images/pemilikkos/".$dPemilik->foto_ktp));
            }
            $delete = $dPemilik =$pk->where("IdPemilik_kos", $id)->delete();
            $status = 1;
            $pesan = "Data berhasil dismpan";
        } catch (Exception $e) {
            //throw $th;
            $status = 2;
            $pesan = "Terjadi Kesalahan ". $e;
        }
        $return = [
            'status'    => $status,
            'pesan'     => $pesan
        ];
        return redirect()->back()->with($return);
    }
}
