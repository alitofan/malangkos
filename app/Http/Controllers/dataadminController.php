<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Exception;
use File;

use App\Models\tbadmin;

class dataadminController extends Controller
{
    
    public function tambahadmin(){
        return view("Backend/dataadmin/dataadminF");
    }

   public function simpanadmin(Request $req, tbadmin $ad, $status = 0, $pesan = "Terjadi Kesalahan"){
        $req->validate([
            "usernm"    => "required",
            "otoritas"  => "required|numeric",
            "pass"      => "required"
        ],[
            "required"          => "Tidak boleh kosong",
            "image"             => "Gambar saja",
            "numeric"           => "Angka saja"
        ]);

        try {

            $insert = $ad->create([
                "username"     => $req->usernm,
                "otoritas"     => $req->otoritas,
                "pass"         => md5($req->pass)
            ]);
            
            $status = 1;
            $pesan = "Data berhasil dismpan";
        } catch (Exception $e) {
            //throw $th;
            $status = 2;
            $pesan = "Terjadi Kesalahan ". $e;
        }
        $return = [
            'status'    => $status,
            'pesan'     => $pesan
        ];
        return redirect()->back()->with($return);
    }

    public function perUpdate($idAdmin, tbadmin $ad){
        $dAdmin = $ad->where("IdAdmin", $idAdmin)->first();
        $return = [
            'admin'   => $dAdmin
        ];
        return view("Backend/dataadmin/dataadminF2", $return);
    }
 public function update1(Request $req, tbadmin $ad, $status = 0, $pesan = "Not Worked"){
        $req->validate([
            "usernm"      => "required",
            "otoritas"    => "numeric",
            "pass"        => "required"
        ],[
            "required"          => "Tidak boleh kosong",
            "numeric"           => "Angka saja"
        ]);

        try {
            $update = $ad->where("IdAdmin", $req->IdAdmin)->update([
                "username"     => $req->usernm,
                "otoritas"     => $req->otoritas,
                "pass"         => md5($req->pass)

            ]);
            $status = 1;
            $pesan = "Data berhasil dismpan";
        } catch (Exception $e) {
            //throw $th;
            $status = 2;
            $pesan = "Terjadi Kesalahan ". $e;
        }

        $return = [
            'status'    => $status,
            'pesan'     => $pesan
        ];
        return redirect()->back()->with($return);
    }
    public function hapus($id, tbadmin $ad, $status = 0, $pesan = "Not Worked"){
        try {
            $dAdmin =$ad->where("IdAdmin", $id)->first();
            
            $delete = $dAdmin =$ad->where("IdAdmin", $id)->delete();
            $status = 1;
            $pesan = "Data berhasil dismpan";
        } catch (Exception $e) {
            //throw $th;
            $status = 2;
            $pesan = "Terjadi Kesalahan ". $e;
        }
        $return = [
            'status'    => $status,
            'pesan'     => $pesan
        ];
        return redirect()->back()->with($return);
    }
       
}
