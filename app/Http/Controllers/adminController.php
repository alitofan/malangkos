<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Exception;
use App\Models\tbpemilikkos;
use App\Models\tbkamar;
use App\Models\tbgalery;
use App\Models\tbadmin;

use App\Models\tbslide;


class adminController extends Controller
{
    //

    public function index(){
        // return view('Backend/dahsboard');
        return view('Backend/dashboard');
    }

    public function Halpemilikkos(tbpemilikkos $pk){

        $return = [
            "pemilik"   => $pk->all()
        ];
        return view('Backend/pemilikkos/pemilikkos', $return);
    }
   

    public function Haldatakamar(tbkamar $kamar){
        $return = [
            "kamar" => $kamar->with("get_pemilik")->get()
        ];
        // dd($return);
        return view('Backend/datakamar/datakamar', $return);
    }

    public function Haldataadmin(tbadmin $ad){
         $return = [
            "admin"   => $ad->all()
        ];
        return view('Backend/dataadmin/dataadmin',  $return);
    }

   public function Halgaleri(tbgalery $tbgalery){
          $return = [
            "tbgalery"   => $tbgalery->all(),
        ];
        return view('Backend/galeri/galeri',  $return);
    }

    public function Halslide(tbslide $tbslide){
        $return = [
            "tbslide"      => $tbslide->all(),
        ];
        return view('Backend/slide/slide', $return);
    }

    public function Halkomplen(){
        return view('Backend/komplen');
    }

    public function Haltestimoni(){
        return view('Backend/testimoni');
    }
}
