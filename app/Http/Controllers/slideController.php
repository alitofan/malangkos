<?php 

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

//conditional
use Exception;
use File;

// model
use App\Models\tbslide;

class slideController extends Controller
{
    //

    public function tambah(){
        return view("Backend/slide/slideF");
    }

    public function simpan(Request $req, tbslide $tbslide, $status = 0, $msg = "Not Worked"){
        $req->validate([
            "title"         => "required",
            "caption"       => "required",
            "gbrSlide"      => "required|image",
            "gbrHasilCrop"  => "required"
        ],[
            "required"      => "Tidak boleh kosong",
            "image"         => "Hanya menerima inputan berupa file gambar"
        ]);
        try {
            $image_array_1 = explode(";", $req->gbrHasilCrop);
            $image_array_2 = explode(",", $image_array_1[1]);
            $data = base64_decode($image_array_2[1]);
            $imgName= date("Ymdhis").'.png';
            $dest = public_path("images/slide/".$imgName);
            file_put_contents($dest, $data);

            $tbslide->create([
                "foto_slide"    => $imgName,
                "caption"       => $req->caption,
                "title"         => $req->title
            ]);
            $status = 1;
            $msg = "Berhasil";
        } catch (Exception $e) {
            $status = 2;
            $msg = "Gagal ". $e;
        }
        $return = [
            "status"    => $status,
            "msg"       => $msg
        ];
        return redirect()->back()->with($return);
        // return "hehe";
    }

    public function perUpdate($id, tbslide $tbslide){
        $return = [
            "tbslide"   => $tbslide->where("IdSlide", $id)->first(),
        ];
        return view("Backend/slide/slideF2", $return);
    }

    public function update1(Request $req, tbslide $tbslide, $status = 0, $msg = "Not Worked"){
        $req->validate([
            "title"     => "required",
            "caption"   => "required"     
        ],[
            "required"  => "Tidak boleh kosong"
        ]);
        try {
            $tbslide->where("IdSlide", $req->idSlide)->update([
                "caption"   => $req->caption,
                "title"     => $req->title
            ]);
            $status = 1;
            $msg = "Berhasil";
        } catch (Exception $e) {
            $status = 2;
            $msg = "Gagal ". $e;
        }
        $return = [
            "status"    => $status,
            "msg"       => $msg
        ];
        return redirect()->back()->with($return);
    }

    public function update2(Request $req, tbslide $tbslide, $status = 0, $mgs = "Not Worked"){
        $dSlide = $tbslide->where("IdSlide", $req->idSlide)->first();
        try {
            $image_array_1 = explode(";", $req->gbrHasilCrop);
            $image_array_2 = explode(",", $image_array_1[1]);
            $data = base64_decode($image_array_2[1]);
            $imgName= date("Ymdhis").'.png';
            $dest = public_path("images/slide/".$imgName);
            file_put_contents($dest, $data);

            if(File::exists(public_path('images/slide/'.$dSlide->foto_slide))){
                File::delete(public_path('images/slide/'.$dSlide->foto_slide));
            }

            $tbslide->where("IdSlide", $req->idSlide)->update([
                "foto_slide"    => $imgName
            ]);

            $status = 1;
            $msg = "Berhasil";
        } catch (Exception $e) {
            $status = 2;
            $msg = "Gagal ". $e;
        }
        $return = [
            "status"    => $status,
            "msg"       => $msg
        ];
        return redirect()->back()->with($return);
    }

    public function hapus($id, tbslide $tbslide, $status = 0, $mgs = "Not Worked"){
        $dSlide = $tbslide->where('IdSlide', $id)->first();
        try {
            if(File::exists(public_path('images/slide/'.$dSlide->foto_slide))){
                File::delete(public_path('images/slide/'.$dSlide->foto_slide));
            }
            $tbslide->where("IdSlide", $id)->delete();
            $status = 1;
            $msg = "Berhasil";
        } catch (Exception $e) {
            $status = 2;
            $msg = "Gagal ".$e;
        }
        $return = [
            "status"    => $status,
            "msg"       => $msg
        ];
        return redirect()->back()->with($return);
    }
}
