-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 26 Apr 2019 pada 10.07
-- Versi server: 10.1.38-MariaDB
-- Versi PHP: 7.3.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dbkos`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbadmin`
--

CREATE TABLE `tbadmin` (
  `IdAdmin` int(11) NOT NULL,
  `username` varchar(20) DEFAULT NULL,
  `otoritas` enum('1','2') DEFAULT '1',
  `pass` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbcomplain`
--

CREATE TABLE `tbcomplain` (
  `IdComplain` int(11) NOT NULL,
  `ket_komplain` text,
  `nm_user` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbgalery`
--

CREATE TABLE `tbgalery` (
  `IdGalery` int(11) NOT NULL,
  `foto_galery` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbgalery`
--

INSERT INTO `tbgalery` (`IdGalery`, `foto_galery`) VALUES
(1, '20190424021949.jpg'),
(2, '20190424022241.png'),
(3, '20190424023029.png');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbkamar`
--

CREATE TABLE `tbkamar` (
  `IdKamar` int(11) NOT NULL,
  `nm_kamar` varchar(100) DEFAULT NULL,
  `hrg_bulanan` int(11) DEFAULT NULL,
  `hrg_persemester` int(11) NOT NULL,
  `hrg_tahunan` int(11) NOT NULL,
  `fasilitas` text,
  `ukuran` text,
  `foto_kamar` text,
  `IdPemilik_kos` int(11) NOT NULL,
  `status` enum('1','2','','') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbpemilikkos`
--

CREATE TABLE `tbpemilikkos` (
  `IdPemilik_kos` int(11) NOT NULL,
  `nm_Pemilikkos` varchar(100) DEFAULT NULL,
  `alamat` varchar(100) DEFAULT NULL,
  `no_telp` varchar(100) DEFAULT NULL,
  `no_wa` varchar(100) DEFAULT NULL,
  `foto_ktp` text,
  `ket_pemilik` text,
  `status` enum('AKTIF','PASIF') DEFAULT 'AKTIF',
  `Idadmin` varchar(20) DEFAULT NULL,
  `foto_perjanjian` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbpemilikkos`
--

INSERT INTO `tbpemilikkos` (`IdPemilik_kos`, `nm_Pemilikkos`, `alamat`, `no_telp`, `no_wa`, `foto_ktp`, `ket_pemilik`, `status`, `Idadmin`, `foto_perjanjian`) VALUES
(2, 'tofan', 'jl. bukirsari', '085648290874', '085648290874', '20190418050404.jpg', 'bla', 'AKTIF', '101', '201904180504042.jpg');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbslide`
--

CREATE TABLE `tbslide` (
  `IdSlide` int(11) NOT NULL,
  `foto_slide` text,
  `caption` text,
  `title` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbtestimoni`
--

CREATE TABLE `tbtestimoni` (
  `IdTesti` int(11) NOT NULL,
  `ket_testi` varchar(255) DEFAULT NULL,
  `nm_user` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `tbadmin`
--
ALTER TABLE `tbadmin`
  ADD PRIMARY KEY (`IdAdmin`);

--
-- Indeks untuk tabel `tbcomplain`
--
ALTER TABLE `tbcomplain`
  ADD PRIMARY KEY (`IdComplain`);

--
-- Indeks untuk tabel `tbgalery`
--
ALTER TABLE `tbgalery`
  ADD PRIMARY KEY (`IdGalery`);

--
-- Indeks untuk tabel `tbkamar`
--
ALTER TABLE `tbkamar`
  ADD PRIMARY KEY (`IdKamar`);

--
-- Indeks untuk tabel `tbpemilikkos`
--
ALTER TABLE `tbpemilikkos`
  ADD PRIMARY KEY (`IdPemilik_kos`);

--
-- Indeks untuk tabel `tbslide`
--
ALTER TABLE `tbslide`
  ADD PRIMARY KEY (`IdSlide`);

--
-- Indeks untuk tabel `tbtestimoni`
--
ALTER TABLE `tbtestimoni`
  ADD PRIMARY KEY (`IdTesti`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `tbadmin`
--
ALTER TABLE `tbadmin`
  MODIFY `IdAdmin` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `tbcomplain`
--
ALTER TABLE `tbcomplain`
  MODIFY `IdComplain` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `tbgalery`
--
ALTER TABLE `tbgalery`
  MODIFY `IdGalery` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `tbkamar`
--
ALTER TABLE `tbkamar`
  MODIFY `IdKamar` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `tbpemilikkos`
--
ALTER TABLE `tbpemilikkos`
  MODIFY `IdPemilik_kos` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `tbslide`
--
ALTER TABLE `tbslide`
  MODIFY `IdSlide` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `tbtestimoni`
--
ALTER TABLE `tbtestimoni`
  MODIFY `IdTesti` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
